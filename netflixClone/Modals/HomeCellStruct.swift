//
//  HomeCellStruct.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import Foundation

// Home model structure
struct HomePageModel {
    
    public var type: CellType = .defaultCell
    public var title = ""
    public var videos: [VideoDetailedModel] = []
}

// Home page cell types
enum CellType {
    
    case defaultCell
    case bannerCell
}

class VideoDetailedModel {
    var title:String = ""
    var imageUrl: String = ""
}

