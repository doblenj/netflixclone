//
//  AlamofireHC.swift
//  
//
//  Created by Ramesh P on 24/10/17.
//  Copyright © 2017 Ramesh P. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

//static let alamofireService = AlamofireHC()

class AlamofireHC: NSObject {
    
    class func requestGET(_ strMethod: String, shouldShowHUD: Bool = true, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        
        //check the network availability
        
        if NetworkManager.sharedInstance.reachability.connection == .none {
            ActivityIndicator.showErrorAlert(StringConstants.noInternetConnectionFound)
            return
        }
        
        if shouldShowHUD {
            
            Common.showNetworkActivity()
        }
        let URL = Constants.baseUrl + strMethod
        
        Alamofire.request(URL).responseJSON { (responseObject) -> Void in
            
            debugPrint(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                debugPrint(resJson)
                if shouldShowHUD {
                    
                    Common.hideNetworkActivity()
                }
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                debugPrint(error)
                if shouldShowHUD {
                    
                    Common.hideNetworkActivity()
                }
                failure(error)
            }
        }
    }
    
    class func requestPOST(_ strMethod : String, params : [String : Any]?, headers : [String : String]?, shouldShowHUD: Bool = true, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        //check the network availability
            
        if NetworkManager.sharedInstance.reachability.connection == .none {
            ActivityIndicator.showErrorAlert(StringConstants.noInternetConnectionFound)
            return
        }
        
        if shouldShowHUD {
            
            Common.showNetworkActivity()
        }
        
        let URL = Constants.baseUrl + strMethod
        
        Alamofire.request(URL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            debugPrint("********************************************")
            debugPrint(URL)
            debugPrint("********************************************")
            debugPrint(responseObject)
            debugPrint("********************************************")
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                debugPrint(resJson)
                if shouldShowHUD {
                    
                    Common.hideNetworkActivity()
                }
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                debugPrint(error)
                if shouldShowHUD {
                    
                    Common.hideNetworkActivity()
                }
                failure(error)
            }
        }
    }
    
    class func requestPOSTwithImage(_ strMethod : String,image : UIImage, params : [String : AnyObject]?, imageParam: String, headers : [String : String]?, shouldShowHUD: Bool = true, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        //check the network availability
        
        if NetworkManager.sharedInstance.reachability.connection == .none {

            ActivityIndicator.showErrorAlert(StringConstants.noInternetConnectionFound)
            return
        }

        if shouldShowHUD {

            Common.showNetworkActivity()
        }

        let URL = Constants.baseUrl + strMethod
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let imageData = image.jpegData(compressionQuality: 0.25) {
                multipartFormData.append(imageData, withName: imageParam, fileName: "image.jpg", mimeType: "image/jpg")
            }
            for (key, value) in params! {
                
                multipartFormData.append((value as AnyObject).data(using:String.Encoding.utf8.rawValue)!, withName: key)
            }},
                         usingThreshold:UInt64.init(),
                         to:URL,
                         method:.post,
                         headers:headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    let json =  JSON(response.result.value!)
                                    debugPrint(json)
                                    if shouldShowHUD {

                                        Common.hideNetworkActivity()
                                    }
                                    success(json)
                                }
                            case .failure(let encodingError):
                                debugPrint(encodingError)
                                if shouldShowHUD {

                                    Common.hideNetworkActivity()
                                }
                                failure(encodingError)
                            }
        })
    }
    
    
    class func requestPOSTwithImage1(_ strMethod : String,image : UIImage,coverimage : UIImage, params : [String : AnyObject]?, headers : [String : String]?, shouldShowHUD: Bool = true, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        //check the network availability
        
//        if NetworkManager.sharedInstance.reachability.connection == .none {
//            
//            ActivityIndicator.showErrorAlert("No internet connection found. Please try again later")
//            return
//        }
//        
//        if shouldShowHUD {
//            
//            Common.showNetworkActivity()
//        }
//        let URL = Constants.baseUrl + strMethod
//        
//        Alamofire.upload(multipartFormData:{ multipartFormData in
//            if let imageData = UIImageJPEGRepresentation(image, 1) {
//                multipartFormData.append(imageData, withName: "picture", fileName: "image.jpg", mimeType: "image/jpg")
//            }
//            if let imageData1 = UIImageJPEGRepresentation(coverimage, 1) {
//                multipartFormData.append(imageData1, withName: "cover", fileName: "image.jpg", mimeType: "image/jpg")
//            }
//            for (key, value) in params! {
//                multipartFormData.append((value as AnyObject).data(using:String.Encoding.utf8.rawValue)!, withName: key)
//            }},
//                         usingThreshold:UInt64.init(),
//                         to:URL,
//                         method:.post,
//                         headers:headers,
//                         encodingCompletion: { encodingResult in
//                            switch encodingResult {
//                            case .success(let upload, _, _):
//                                upload.responseJSON { response in
//                                    
//                                    let json =  JSON(response.result.value!)
//                                    debugPrint(json)
//                                    if shouldShowHUD {
//                                        
//                                        Common.hideNetworkActivity()
//                                    }
//                                    success(json)
//                                    
//                                }
//                            case .failure(let encodingError):
//                                debugPrint(encodingError)
//                                if shouldShowHUD {
//                                    
//                                    Common.hideNetworkActivity()
//                                }
//                                failure(encodingError)
//                            }
//        })
    }
    
}






