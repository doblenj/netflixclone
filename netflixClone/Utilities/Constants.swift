//
//  Constants.swift
//  ICH
//
//  Created by Doble on 20/08/17.
//  Copyright © 2017 BTeem. All rights reserved.
//

import Foundation
import UIKit

var gradientLayer: CAGradientLayer!

struct Constants {
    
    static let baseUrl = ""
    static let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
    static let currencyCode = "$"
    
    struct Keys {
        
        // User logged in status
        static let isLoggedIn     = "is_logged_in"
        // Current user type
        static let loginTypeKey    = "login_type"
        // User's current profile image
        static let userPicKey     = "user_profile_image"
        // User name
        static let userNameKey    = "username"
        // Email address
        static let emailIdKey = "ich_email"
        
        // User phone num
        static let phoneNumKey = "phone_num"
        
        //push and email noti settings'
        static let pushNotiStatus = "push_noti_status"
        static let emailNotiStatus = "email_noti_status"
    }

    struct SegueIds {
        
        static let homeVC = "showHomeVC"
        static let loginVC = "showLoginVC"
    }

    struct ViewControllers {
        
        //launch page
        static let launchVC = "LaunchVC"
        
        //login pages
        static let loginVC = "SignInVC"
        static let registerVC = "SignUpVC"
        static let changePwdVC = "ChangePasswordVC"
        static let forgotPwdVC = "ForgotPasswordVC"
        static let updateProfileVC = "UpdateProfileVC"
        static let whoIsWatchingVC = "WhoIsWatchingVC"
        
        //baseVC
        static let baseVC = "BaseTabBarController"
        
        //tabbar main pages
        static let homeVC = "HomeVC"
        static let searchVC = "SearchVC"
        static let categoriesVC = "CategoriesVC"
        static let moreVC = "MoreVC"
        
        //Single video page
        static let singleVC = "SingleVideoVC"
        static let playerVC = "PlayerVC"
        
        //****Tabbar Sub pages****
        //SettingsSub
        static let manageProVC = "ManageProVC"
        static let editProfileVC = "EditProfileVC"
        static let appSettingsVC = "AppSettingsVC"
        static let staticPagesVC = "StaticPagesVC"
        static let accountVC = "AccountVC"
        static let viewPlansVC = "ViewPlansVC"
        static let viewPlanDetailsVC = "ViewPlanDetailsVC"
        static let paymentsVC = "PaymentsVC"
        static let invoiceVC = "InvoiceVC"
        static let myPlansVC = "MyPlansVC"
        static let myPlanDetailsVC = "MyPlanDetailsVC"
        static let cardsVC = "CardsVC"
        static let videosListCommonVC = "VideosListCommonVC"
        static let paidVideosVC = "PaidVideosVC"
        static let editAccountVC = "EditAccountVC"
        static let notificationVC = "NotificationVC"
        static let myWalletVC = "MyWalletVC"
        static let walletSuccesfullVC = "WalletSuccesfullVC"
        static let wallletDetailVC = "WallletDetailVC"
        
        // SeeAll page
        static let seeAllVC = "SeeAllVC"
        
        // Category detail
        static let categoryDetailedVC = "CategoryDetailedVC"
    }
    
    struct Cells {
        
        //TableView cells
        static let moreCell = "MoreCell"
        static let appSettingsCell = "AppSettingsCell"
        static let accountCell = "AccountCell"
        static let viewPlansCell = "ViewPlansCell"
        static let myPlansCell = "MyPlansCell"
        static let cardsCell = "CardsCell"
        static let homeBannerTCell = "HomeBannerCell"
        static let homeCommonCell = "HomeCommonCell"
        static let videosListTCell = "VideosListCommonCell"
        static let notificationCell = "NotificationCell"
        static let walletCell = "WalletCell"
        
        //Single video table cells:
        static let playerTableCell = "PlayerTableCell"
        static let detailedTableCell = "DetailedTableCell"
        static let utilityTableCell = "UtilityTableCell"
        static let trailerTableCell = "TrailersTableCell"
        static let seasonTableCell = "SeasonTableCell"
        static let moreLikeThisTableCell = "MoreLikeThisTableCell"
        
        //CollectionView cell
        static let moreCCell = "MoreCCell"
        static let manageCCell = "ManageCCell"
        static let homeDefaultCCell = "HomeDefaultCollectionCell"
        static let categoryCollectionCell = "CategoriesCollectionCell"
        static let seeaAllCCell = "SeeAllCollectionCell"
        static let whoIsWatchingCCell = "WhoIsWatchingCCell"
        static let homeBannerCCell = "HomeBannerCollectionCell"
    }
    
    struct ViewXibs {
        static let singleVideoHeaderView = "SingleVideoHeaderView"
    }
    
    /// theame Color of app
    struct CommonColors {
        
        //color for netflixClone
        static let theameLight : UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        static let theameDark : UIColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        static let theameMix : UIColor = UIColor(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0)
    }
    
    
    /// common dateformat
    struct dateFormat {
        
        static let dateFormat = "yyyy-MM-dd"
    }

    //notifications
    struct Notifications {
        
        static let networkConnected = "network_connected"
        static let networkDisconnected = "network_diconnected"
        static let updateCartBadge = "update_cart_badge"
        static let shortcutAction      = "quick_action_notification"
        static let openQuickOrder = "quick_order_action"
    }
    
    //storybord ids
    struct StoryboardIds {
        
        static let mainSb = "Main"
        static let loginSb = "Login"
        static let homeSb = "Home"
        static let moreSubSb = "MoreSub"
    }
    
    //urls for api call
    struct Urls {
        
    }
    
}
