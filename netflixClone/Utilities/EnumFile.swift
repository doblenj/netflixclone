//
//  EnumFile.swift
//  netflixClone
//
//  Created by Doble on 02/04/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import Foundation

enum PageType: String {
    
    case home = "HOME"
    case series = "SERIES"
    case films = "FILMS"
    case kids = "KIDS"
}

