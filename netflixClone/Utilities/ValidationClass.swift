//
//  ValidationClass.swift
//  StreamTunes
//

import UIKit

class ValidationClass: NSObject {

    class func isValidEmailID(email:String) ->Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = email as NSString
            let results = regex.matches(in: email, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            debugPrint("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    class func isValidPassword(password:String) ->Bool {
        
        if password.count < 6 {
            
            return false
        } else {
        
            return true
        
        }
        
    }
    
    class func isPasswordMatching(password : String , confirmPassword : String) -> Bool {
    
        
        if password == confirmPassword {
        
            return true
      
        } else {
        
            return false
        }
    
    }
    
    
    
    /// To check whether input string is valid
    ///
    /// - Parameter inputStr: string to check
    /// - Returns: true if valid
    class func isValidString(inputStr : String) -> Bool {
    
        let rawString: NSString = inputStr as NSString
        let whitespace: NSCharacterSet = NSCharacterSet.whitespacesAndNewlines as NSCharacterSet
        let trimmed = rawString.trimmingCharacters(in: whitespace as CharacterSet)
        
        if trimmed.count > 0 {
            
            return true
        } else {
            
            return false
        }
    }
    
    
    
    /// To validate phone number given
    ///
    /// - Parameter value: input number
    /// - Returns: true if valid
    class func validatePhoneNumber(value: String) -> Bool {
        
        let regexNumbersOnly = try! NSRegularExpression(pattern: ".*[^0-9].*", options: [])
        return regexNumbersOnly.firstMatch(in: value, options: [], range: NSMakeRange(0, value.count)) == nil && value.count >= 10
    }
    
    /// to check for the error code from backend and forcelogout if so
    ///
    /// - Parameter errorCode: errorcode from response
    /// - Returns: true or false
    class func shouldForceLogoutForErrorCode(errorCode: Int) -> Bool {
        
        switch errorCode {
            
        case 3000:
            return true
        case 103:
            return true
        case 104:
            return true
        
        default:
            return false
        }
    }
      
}
