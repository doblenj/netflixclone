//
//  StringConstants.swift
//  netflixClone
//
//  Created by Doble on 06/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import Foundation
struct StringConstants {
    
    //MARK: All the static sentences or words inside the codes are tken from here. this file can be directly used if you are doing localization for this project.
    
    //Common
    
    static let pleaseTryAgain = "Please try Again"
    static let confirm = "Confirm"
    static let emailAddressIsNotValid = "Email address is not valid"
    static let cancel = "Cancel"
    static let noInternetConnectionFound = "No internet connection found. Please try again later"
    static let somethingWentWrong = "Something went wrong"
    static let pleaseInputYourEmail = "Please input your email:"
    static let passwordCharacterCountError = "Password must have minimum 6 characters"
    static let pleaseGiveAValidEmailAddress = "Please give a valid email address"
    static let sessionExpired = "Oops! Your session is expired,Please login again."
    
    //ManageProVC
    static let addProfile = "Add Profile"
    
    //AppSettingsVC
    struct notiArray {
        
        static let pushNotifications = "Push Notifications"
        static let emailNotifications = "Email Notifications"
    }
    
    struct legalArray {
        
        static let privacyPolicy = "Privacy Policy"
        static let termsOfUse = "Terms of Use"
    }
    
    struct sectionArray {
        
        static let notifications = "Notifications"
        static let legal = "Legal"
    }
    
    struct proTableArray {
        
        static let appSettings = "App Settings"
        static let account = "Account"
        static let notifications = "Notifications"
        static let logOut = "Log Out"
    }
    
    //ChangePasswordVC
    static let newPasswordsDoNotMatch = "New passwords do not match"
    static let newPasswordIsNotValid = "New password is not valid"
    static let pleaseEnterAValidPassword = "Please enter a valid Password"
    static let fieldsCantBeEmpty = "Fields can't be empty"
    
    //SignInVC
    static let email = "Email"
    static let help = "Help"
    
    //AccountVc
    struct AccountArray {
        
        static let viewPlans = "View Plans"
        static let myPlans = "My Plans"
        static let wishList = "Wish List"
        static let cards = "Cards"
        static let myWallet = "My Wallet"
        static let paidVideos = "Paid Videos"
        static let spamVideos = "Spam Videos"
        static let history = "History"
        static let deleteAccount = "Delete Account"
        static let changePassword = "Change Password"
    }
    
    //ViewPlanDetailsVC
    static let pay = "PAY"
    static let select = "SELECT"
    
    //PaymentsVC
    static let payments = "Payments"
    static let enterCode = "Please enter a coupon code"
    static let sureToPayAmount = "Are you sure to pay the amount of "
    static let sureToPayAmountFromWallet = "Are you sure to pay from the wallet an amount of "
    static let sureToAddToWallet = "Are you sure to add to wallet an amount of "
    
    //InvoiceVC
    static let invoice = "Invoice"
    
    //CardsVC
    static let cards = "Cards"
    
    //EditAccountVC
    static let editAccount = "Edit Account"
    static let chooseImage = "Choose Image"
    static let camera = "Camera"
    static let gallery = "Gallery"
    static let warning = "Warning"
    static let youDontHaveCamera = "You don't have camera"
    static let ok = "OK"
    static let youDontHavePerissionToAccessGallery = "You don't have permission to access gallery."
    static let updatedSuccessfully = "Updated successfully"
    
    //ManageProVC
    static let manageProfile = "Manage Profile"
    
    //EditProfileVC
    static let editProfile = "Edit Profile"
    
    //AppSettingsVC
    static let appSettings = "App Settings"
    
    //AccountVC
    static let account = "Account"
    static let areYouSureToDeleteAccount = "Your account will be deleted and cannot be undone"
    static let passwordIsRequired = "Password is required"
    static let enterPasswordToDelete = "Enter your password to delete the acount"
    
    //WhoIsWatchingPage
    static let whoIsWatchingPage = "Who is watching?"
    
    //MoreVC
    static let areYouSureToLogout = "Are you sure to logout?"
    static let yes = "YES"
    static let no = "NO"
    static let disableAutoRenewal = "Are you sure you want to disable auto renewal?"
    static let enableAutoRenewal = "Are you sure you want to enable auto renewal?"
    static let sureToDeleteSubProfile = "Are you sure to delete this subprofile?'"

    //VideoListCommonVC
    
    static let sureToClearAll = "Are you sure to clear all videos?"
    static let sureToDeleteThisVideo = "Are you sure to delete this video?"
    static let removeFromSpamToPlay = "Please remove from spam to play this video"
    
    //CategoriesVC
    static let categories = "Categories"
    
    //SearchVC
    static let search = "Search"
    
    //Offline videos
    static let downloads = "Downloads"
    static let download = "Download"
    static let downloadLinkNotAvailable = "Download link is not available"
    static let downloaded = "Downloaded"
    static let downloading = "Downloading"
    static let removeFromSpam = "This video is added in spam list. Please remove from spam to play this video"
    static let play = "Play"
    static let delete = "Delete"
    static let cancelDownload = "Download cancelled"
    static let deletedSuccessfully = "Song deleted successfully"
    
    //Single video vc
    static let reportSpam = "Report as spam ?"
    static let reasonForSpam = "Reason for spam"
    static let moreLikeThese = "MORE LIKE THESE"
    static let browseSeasons = "BROWSE SEASONS"
    static let trailersAndMore = "TRAILERS AND MORE"
    static let likes = "Likes"
    static let downloadStarted = "Download started"
    static let downloadCompleted = "Download completed"
    
    //HomeVC
    static let series = "Series"
    static let kids = "Kids"
    static let films = "Films"
    
    //AdMoneyVC
    
    static let addMoney = "Enter amount to be added"
    static let addVoucher = "Enter Voucher code"
    static let orVoucher = "Got a Voucher?"
    static let orMoney = "Add money to wallet"
    static let paypalTitle = "Add money to wallet"
    static let pleaseEnterAValidAmount = "Please Enter a Valid Amount"
    static let pleaseEnterAValidCode = "Please Enter a Valid Code"
    static let used = "Used:"
    
    static let pending = "Pending"
    static let staticPage = "Static page"
}
