//
//  GeneralSingleton.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import Foundation

class GeneralSingleton {
    
    static let shared = GeneralSingleton()
    
    private init() {}

    
    //to dismiss to viewplansdVc
    var isFromPlansSuccssfullPayment : Bool = false
    var goingToDetailsFromInvoice : Bool = false
    
}
