//
//  NetworkManager.swift
//  ICH
//
//  Created by Doble on 20/08/17.
//  Copyright © 2017 BTeem. All rights reserved.
//

import UIKit
import Reachability

class NetworkManager: NSObject {

    
    enum NANetworkStatus  {
        
        case networkConnected
        
        case networkDisconnected
    }
    
    //declare this property where it won't go out of scope relative to your listener
    
    let reachability = Reachability()!
    
    let networkStatus = NANetworkStatus?.self
    
    
    //MARK: Shared Instance
    
    static let sharedInstance : NetworkManager = {
        
        let instance = NetworkManager()
        
        return instance
        
    }()
    
    override init(){
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        
        do{
            
            try reachability.startNotifier()
            
        }catch{
            
            debugPrint("could not start reachability notifier")
            
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        checkReachability(reachability: reachability)
        
    }
    
    func checkReachability (reachability : Reachability){
        
        if reachability.isReachable {
            
            if reachability.isReachableViaWiFi {
                
                debugPrint("Reachable via WiFi")
                
            } else {
                
                debugPrint("Reachable via Cellular")
            }
            
            // Post notification
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.networkConnected), object: nil)
            
        } else {
            
            // Post notification
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.networkDisconnected), object: nil)
            
        }
    }

    
}
