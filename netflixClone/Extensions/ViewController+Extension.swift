//
//  ViewControllerExtension.swift
//  StreamTunes
//
//  Created by Doble on 21/01/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer
import Toast_Swift

public extension UIViewController  {
    
    //to remove the word "Back" from all the navigation back button items
    open override func awakeFromNib() {
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backButon")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backButon")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
    }
    
    //isForcefull = false for normal logout
    func performLogout(msg: String = "", Vc: UIViewController, isForcefull : Bool = true)  {
        
        if isForcefull {
            
            let alert = UIAlertController(title: Constants.appName, message: StringConstants.sessionExpired, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: StringConstants.ok, style: UIAlertAction.Style.default) {
                
                (result : UIAlertAction) -> Void in
                
                self.logOutDeleteData()
            }
            alert.addAction(okAction)
            Vc.present(alert, animated: true, completion: nil)
        } else {
            
            self.logOutDeleteData()
        }
    }
    
    //clear the data on logout
    func logOutDeleteData() {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Constants.Keys.isLoggedIn)
        defaults.removeObject(forKey: Constants.Keys.loginTypeKey)
        defaults.removeObject(forKey: Constants.Keys.userPicKey)
        defaults.removeObject(forKey: Constants.Keys.userNameKey)
        defaults.removeObject(forKey: Constants.Keys.emailIdKey)
        defaults.removeObject(forKey: Constants.Keys.phoneNumKey)
        defaults.removeObject(forKey: Constants.Keys.pushNotiStatus)
        defaults.removeObject(forKey: Constants.Keys.emailNotiStatus)
        
        defaults.synchronize()
        navigateToRootVC()
    }
    
    //to Navigate to root VC
    
    func navigateToRootVC () {

        let story = UIStoryboard.init(name: Constants.StoryboardIds.mainSb, bundle: nil)
        if let vC = story.instantiateViewController(withIdentifier: Constants.ViewControllers.launchVC) as? LaunchVC {
            
            let navigationController = UINavigationController(rootViewController: vC)
            self.view.window?.rootViewController = navigationController
            self.view.window?.makeKeyAndVisible()
        }
    }
    
    // Navigate to single video page
    func navigateToSingleVideoPage(adminVideoID: Int) {
        
        let storyBoard = UIStoryboard(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.singleVC) as? SingleVideoVC {
            
            vc.videoID = adminVideoID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /// func to delay in main thread
    ///
    /// - Parameters:
    ///   - seconds: seconds to delay
    ///   - dispatchLevel: dispatch level
    ///   - closure: success closure
    func delay(bySeconds seconds: Double, dispatchLevel: DispatchLevel = .main, closure: @escaping () -> Void) {
        
        let dispatchTime = DispatchTime.now() + seconds
        dispatchLevel.dispatchQueue.asyncAfter(deadline: dispatchTime, execute: closure)
    }
    
    public enum DispatchLevel {
        
        case main, userInteractive, userInitiated, utility, background
        var dispatchQueue: DispatchQueue {
            
            switch self {
                
            case .main:                 return DispatchQueue.main
            case .userInteractive:      return DispatchQueue.global(qos: .userInteractive)
            case .userInitiated:        return DispatchQueue.global(qos: .userInitiated)
            case .utility:              return DispatchQueue.global(qos: .utility)
            case .background:           return DispatchQueue.global(qos: .background)
            }
        }
    }
    
    func backBtnTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   
}
