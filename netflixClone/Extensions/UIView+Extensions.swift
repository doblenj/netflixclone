
//
//  UIView+Extensions.swift
//  AMRMS
//
//  Created by DOBLE N J on 20/05/17.
//  Copyright © 2017 Bteem. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DesignableView: UIView {
}

extension UIView {
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    
    
    /// To set a shadow
    ///
    /// - Parameters:
    ///   - color: shadow color
    ///   - offset: shadow offset
    ///   - opacity: shadow opacity
    ///   - radius: shadow radius
    
    func setShadowProperties(color: UIColor, offset: CGSize, opacity: Float, radius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    
    /// To set border and corner radius
    ///
    /// - Parameters:
    ///   - borderColor: borderColor
    ///   - borderWidth: borderWidth
    ///   - cornerRadius: cornerRadius
    ///   - clipsToBounds: masksToBounds
    func setBorderProperties(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat, masksToBounds: Bool) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = masksToBounds
    }
    
    
    /// to set the shadow
    ///
    /// - Parameters:
    ///   - color: color
    ///   - opacity: opacity
    ///   - offSet: offset
    ///   - radius: radius
    ///   - scale: scale
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, cornerRadius: CGFloat = 5, radius: CGFloat = 1, scale: Bool = true) {
        
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.cornerRadius = cornerRadius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    /// to give a gradient view
    ///
    /// - Parameter uiView: view in which the gradient view needs
    func addDefaultGradiantLayer (uiView: UIView) {
        
        let colorTop =  UIColor.black
        let colorBottom = UIColor.black
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
  
}


