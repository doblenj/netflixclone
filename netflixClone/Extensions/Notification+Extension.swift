//
//  Notification+Extension.swift
//  StreamTunes
//
//  Created by Doble on 15/02/19.
//  Copyright © 2019 CodeGama. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {

    static let showPaymentView = Notification.Name("showPaymentView")
    static let updateLikesCount = Notification.Name("updateLikesCount")
}

