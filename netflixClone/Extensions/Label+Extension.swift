
//
//  Label+Extension.swift
//  StreamTunes
//
//  Created by Doble on 11/02/19.
//  Copyright © 2019 CodeGama. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
   
    func calculateNumOfLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
