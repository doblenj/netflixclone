//
//  BaseViewController.swift
//  ICH
//
//  Created by Doble on 20/08/17.
//  Copyright © 2017 BTeem. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Navigation type left button
    func addLeftBackButton() {
        
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setTitle("Back",for: .normal)
        btnLeftMenu.setTitleColor(UIColor.black, for: .normal)
        btnLeftMenu.addTarget(self, action:#selector(backButtonTapped(sender:)), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: -20, y: 32, width: 100, height: 22)
        self.view.addSubview(btnLeftMenu)
    }
    
    /// Back button tapped -- Pop Viewcontroller
    ///
    /// - Parameter sender: button action
    @objc func backButtonTapped(sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: KeyBoard Notifications
    /// Adding keyboard observers when the keyboard is presented
//    func addKeyBoardObservers() {
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIResponder.keyboardWillHideNotification, object: nil)
//    }
    
    /// Removing keyboard observers when keyboard dismissed
    func removeKeyBoardObserVers() {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    /// Keyboard appered - Shift view
    ///
    /// - Parameter notification: nsnotification
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= (keyboardSize.height - 50)
            }
        }
    }
    
    
    /// Keyboard dismissed - Shift view downwards
    ///
    /// - Parameter notification: nsnotification
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y = 0
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
