//
//  BaseTabBarController.swift
//  ICH
//
//  Created by Doble on 02/09/17.
//  Copyright © 2017 BTeem. All rights reserved.
//

import UIKit


class BaseTabBarController: UITabBarController {

   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.backgroundColor = Constants.CommonColors.theameDark
        self.tabBar.tintColor = Constants.CommonColors.theameLight
        // Do any additional setup after loading the view.
        
        
//         NotificationCenter.default.addObserver(self, selector: #selector(self.updateCartBadge(_:)),name: NSNotification.Name(rawValue:ICHConstants.Notifications.updateCartBadge),object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.shortcutKeyAction(_:)),name: NSNotification.Name(rawValue:ICHConstants.Notifications.shortcutAction),object: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setCartBadgeCount()
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateCartBadge(_ notification : NSNotification) {
        
        setCartBadgeCount()
    }
    
    
    
    
    func setCartBadgeCount() {
        
//        if RentalDataHelper.getCartCount() != "" && RentalDataHelper.getCartCount() != "0"{
//
//            DispatchQueue.main.async {
//
//                self.tabBar.items?[1].badgeValue = ICHDataHelper.getCartCount()
//
//            }
//
//        } else {
//
//            DispatchQueue.main.async {
//
//                self.tabBar.items?[1].badgeValue = nil
//            }
//        }
    }

}
