//
//  ForgotPasswordVC.swift
//  Rental Provider
//
//  Created by Doble on 12/01/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordVC: RentalBaseViewController {

    
    //MARK: Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var sendMailUIView: UIView!
    @IBOutlet weak var sendMailBtn: UIButton!
    
    //MARK: Actions
    
    @IBAction func sendMailBtnTapped(_ sender: Any) {
        
        if RentalValidationClass.isValidEmailID(email: emailTextField.text ?? "") {
        
            let paramDic = ["email": self.emailTextField.text ?? ""]
            
            AlamofireHC.requestPOST(RentalConstants.Urls.forgotPasswordUrl, params: paramDic, headers: nil, success: { (response) in
                
                let  result = response.dictionaryObject
                
                let resultcheck = result!["success"] as! Bool
                
                if(resultcheck){
                    
                    let msg = result!["message"] as! String
                    self.view.makeToast(msg)
                } else {
                    
                    let errorCode: Int = result!["error_code"] as? Int ?? 0
                    let msg = result!["error"] as? String ?? ""
                    
                    if RentalValidationClass.shouldForceLogoutForErrorCode(errorCode: errorCode) {
                        
                        self.performLogout(Vc: self)
                    } else {
                        
                        self.view.makeToast(msg)
                    }
                }
                
            }) { (error) in
                self.view.makeToast("Please try Again")
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUPUI()
        self.addLeftBackButton()
    }
    

    override func backButtonTapped(sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func setUPUI() {
        
        self.sendMailBtn.backgroundColor = RentalConstants.CommonColors.pinkColor
        self.sendMailUIView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 5, masksToBounds: true)
    }

}
