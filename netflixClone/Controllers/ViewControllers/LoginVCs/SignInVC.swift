//
//  SignInVC.swift
//  Rental User
//
//  Created by Doble on 07/01/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class SignInVC: UIViewController {
    
    //MARK: Variables
    
    let defaults = UserDefaults.standard

    //MARK: Outlets
    
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginBtnView: UIView!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    //MARK: for color regardingOutlets
    @IBOutlet var viewDark: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewOfScroll: UIView!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet weak var helpLbl: UIButton!
    @IBOutlet weak var googleBtnView: UIButton!
    
    //MARK: Action
    
    @IBAction func signUpBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.loginSb, bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.registerVC) as? SignUpVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func helpBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.staticPagesVC) as? StaticPagesVC {
            
            vc.heading = StringConstants.help
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func googleBtnTapped(_ sender: Any) {
        
        defaults.set("social", forKey: Constants.Keys.loginTypeKey)
        self.navigateToWhoisWatching()
    }
    
    @IBAction func facebookBtnTapped(_ sender: Any) {
        
        defaults.set("social", forKey: Constants.Keys.loginTypeKey)
        self.navigateToWhoisWatching()
    }
    
    //MARK: Manual Login Btn tapped
    @IBAction func loginBtnTapped(_ sender: Any) {
        
        if isValidateParameters() {
            
            defaults.set("manual", forKey: Constants.Keys.loginTypeKey)
            self.navigateToWhoisWatching()
        }
    }
    
    
    @IBAction func forgotPasswodBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.loginSb, bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.forgotPwdVC) as? ForgotPasswordVC {
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func passwordBtnTapped(_ sender: Any) {
        
        self.passwordBtn.isSelected = !self.passwordBtn.isSelected
        self.passwordTextField.isSecureTextEntry = self.passwordBtn.isSelected ? false : true
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.setUpColor()
    }
    
    func updateUI() {
        
        self.loginBtnView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
    }
    
    func setUpColor() {
        
        //DARK
        self.viewDark.backgroundColor = Constants.CommonColors.theameDark
        self.scrollView.backgroundColor = Constants.CommonColors.theameDark
        self.viewOfScroll.backgroundColor = Constants.CommonColors.theameDark
        self.socialView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.orLbl.textColor = Constants.CommonColors.theameLight
        self.helpLbl.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.loginButton.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.forgotPasswordButton.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.signUpBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }
    
    func navigateToWhoisWatching() {
        
        defaults.set(self.emailTextField.text ?? "email@email.com", forKey: Constants.Keys.emailIdKey)
        defaults.set("profile name", forKey: Constants.Keys.userNameKey)
        defaults.set("00000000", forKey: Constants.Keys.phoneNumKey)
        defaults.set(1, forKey: Constants.Keys.pushNotiStatus)
        defaults.set(1, forKey: Constants.Keys.emailNotiStatus)
        defaults.synchronize()
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.loginSb, bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.whoIsWatchingVC) as? WhoIsWatchingVC {
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    //to validate the parameters
    
    func isValidateParameters() -> Bool {
        
        if emailTextField.text == "" || emailTextField.text == nil || passwordTextField.text == "" || passwordTextField.text == nil {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
            return false
            
        } else {
            
            if ValidationClass.isValidEmailID(email: self.emailTextField.text ?? "") {
            
                if ValidationClass.isValidPassword(password: passwordTextField.text ?? "") {
                    
                    return true
                } else {
                    
                   self.view.makeToast(StringConstants.passwordCharacterCountError)
                    return false
                }
            } else {
               
                self.view.makeToast(StringConstants.pleaseGiveAValidEmailAddress)
                return false
            }
        }
    }
    
}
