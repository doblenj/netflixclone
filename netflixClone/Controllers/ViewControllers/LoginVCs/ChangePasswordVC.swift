//
//  ChangePasswordVC.swift
//  Rental User
//
//  Created by Doble on 08/02/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordVC: UIViewController {

    
    //MARK: Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var currentPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var currentPasswordShowBtn: UIButton!
    @IBOutlet weak var newPassowrdTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordBtn: UIButton!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordBtn: UIButton!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var changePasswordBtnUIView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var changePLbl: UILabel!
    
    //MARK: Actions
    

    @IBAction func currentPShowBtnTapped(_ sender: Any) {
        
        self.currentPasswordShowBtn.isSelected = !self.currentPasswordShowBtn.isSelected
        self.currentPasswordTextField.isSecureTextEntry = self.currentPasswordShowBtn.isSelected ? false : true
    }
    
    @IBAction func newPShowBtnTapped(_ sender: Any) {
        
        self.newPasswordBtn.isSelected = !self.newPasswordBtn.isSelected
        self.newPassowrdTextField.isSecureTextEntry = self.newPasswordBtn.isSelected ? false : true
    }
    
    @IBAction func confirmPShowBtnTapped(_ sender: Any) {
        
        self.confirmPasswordBtn.isSelected = !self.confirmPasswordBtn.isSelected
        self.confirmPasswordTextField.isSecureTextEntry = self.confirmPasswordBtn.isSelected ? false : true
    }
    
    @IBAction func changePasswordBtnTapped(_ sender: Any) {
        
        if currentPasswordTextField.text != "" && newPassowrdTextField.text != "" && confirmPasswordTextField.text != "" {
            
            if ValidationClass.isValidPassword(password: currentPasswordTextField.text ?? "") {
                
                if ValidationClass.isValidPassword(password: newPassowrdTextField.text ?? "") {
                    
                    if newPassowrdTextField.text == confirmPasswordTextField.text {
                        
                        self.performChangePassword()
                    } else {
                        
                        self.view.makeToast(StringConstants.newPasswordsDoNotMatch)
                    }
                    
                } else {
                    
                    self.view.makeToast(StringConstants.newPasswordIsNotValid)
                }
            } else {
                
                self.view.makeToast(StringConstants.pleaseEnterAValidPassword)
            }
        } else {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
         self.changePasswordBtnUIView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
        self.setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.changePLbl.textColor = Constants.CommonColors.theameLight
        self.changePasswordBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }
    
    /// api method to change the password
    func performChangePassword() {
        
        self.backBtnTapped()
    }
   
}
