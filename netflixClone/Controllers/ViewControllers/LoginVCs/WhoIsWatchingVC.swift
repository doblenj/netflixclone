//
//  WhoIsWatchingVC.swift
//  netflixClone
//
//  Created by Doble on 12/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class WhoIsWatchingVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var whoIsWLbl: UILabel!
    
    //MARK: Actions
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.whoIsWatchingPage
        setUpColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.whoIsWLbl.textColor = Constants.CommonColors.theameLight
    }
    
    
    
    //MARK: CollectionView delegates
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.whoIsWatchingCCell, for: indexPath as IndexPath) as? WhoIsWatchingCCell {
            
            cell.configureMoreCellWith(indexPath: indexPath)
            return cell
            
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        UserDefaults.standard.set(true, forKey: Constants.Keys.isLoggedIn)
        self.navigateToRootVC()
    }
    

}
