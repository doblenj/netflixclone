//
//  ForgotPasswordVC.swift
//  Rental Provider
//
//  Created by Doble on 12/01/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordVC: BaseViewController {

    
    //MARK: Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var sendMailUIView: UIView!
    @IBOutlet weak var sendMailBtn: UIButton!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var enterNameLbl: UILabel!
    
    
    //MARK: Actions
    
    @IBAction func backbtnTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMailBtnTapped(_ sender: Any) {
        
        if isValidateParameters() {
            
           self.backBtnTapped()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUPUI()
        setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.enterNameLbl.textColor = Constants.CommonColors.theameLight
        self.sendMailBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }

    func setUPUI() {
        
        self.sendMailUIView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
    }
    
    //To validate the parameters
    func isValidateParameters() -> Bool {
        
        if emailTextField.text == "" || emailTextField.text == nil   {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
            return false
            
        } else {
            
            if ValidationClass.isValidEmailID(email: self.emailTextField.text ?? "") {
                
                return true
            } else {
                
                self.view.makeToast(StringConstants.pleaseGiveAValidEmailAddress)
                return false
            }
        }
        
    }
    
}
