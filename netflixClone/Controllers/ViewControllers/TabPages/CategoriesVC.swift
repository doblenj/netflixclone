//
//  CategoriesVC.swift
//  netflixClone
//
//  Created by Doble on 28/02/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SDWebImage
import KRPullLoader

class CategoriesVC: UIViewController, KRPullLoadViewDelegate {

    // Outlets
    @IBOutlet weak var categoryCollections: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var nothingFoundLbl: UILabel!
    
    
    //Variables
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.categories
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        let loadMoreView = KRPullLoadView()
        refreshView.delegate = self
        categoryCollections.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        categoryCollections.addPullLoadableView(loadMoreView, type: .loadMore)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        setUpColor()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.categoryCollections.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.nothingFoundLbl.textColor = Constants.CommonColors.theameLight
       
    }
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            
            switch state {
                
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    
                    completionHandler()
                    
                    self.view.makeToast(StringConstants.pending)
                }
            default: break
            }
            return
        }
        
        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }
}

extension CategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            return CGSize(width: UIScreen.main.bounds.width/2-16, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.categoryCollectionCell, for: indexPath) as? CategoriesCollectionCell {
        
            cell.collectionTitleLbl.text = "Title comes here"
            cell.collectionImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            
            return cell
        } else {
        
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        if let detailedVC = storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.categoryDetailedVC) as? CategoryDetailedVC {

            self.navigationController?.pushViewController(detailedVC, animated: true)
        }
    }
    
}
