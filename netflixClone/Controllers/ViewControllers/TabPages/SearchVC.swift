
//
//  SearchVC.swift
//  netflixClone
//
//  Created by Doble on 28/02/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, UISearchBarDelegate {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchCollection: UICollectionView!
    @IBOutlet weak var screenPlaceholderUIView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var findWhatLbl: UILabel!
    @IBOutlet weak var searchForLbl: UILabel!
    @IBOutlet weak var nothingFoundLbl: UILabel!
    @IBOutlet weak var collLbl: UILabel!
    
    
    //MARK: Actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchCollection.register(UINib(nibName: Constants.Cells.homeDefaultCCell, bundle: nil), forCellWithReuseIdentifier: Constants.Cells.homeDefaultCCell)
        searchCollection.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        searchCollection.isHidden = true
        self.emptyView.isHidden = true
        screenPlaceholderUIView.isHidden = false
        setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.screenPlaceholderUIView.backgroundColor = Constants.CommonColors.theameDark
        self.searchCollection.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.findWhatLbl.textColor = Constants.CommonColors.theameLight
        self.searchForLbl.textColor = Constants.CommonColors.theameLight
        self.nothingFoundLbl.textColor = Constants.CommonColors.theameLight
        self.searchBar.tintColor = Constants.CommonColors.theameLight
        self.collLbl.textColor = Constants.CommonColors.theameLight
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchCollection.reloadData()
        self.screenPlaceholderUIView.isHidden = true
        self.emptyView.isHidden = true
        self.searchCollection.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchCollection.isHidden = true
        self.screenPlaceholderUIView.isHidden = false
        self.emptyView.isHidden = true
        self.searchBar.text = ""
        self.searchCollection.reloadData()
        self.searchBar.resignFirstResponder()
    }
}

//MARK: CollectionView delegate and data source

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width/3-16, height: UIScreen.main.bounds.width/3+30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.homeDefaultCCell, for: indexPath) as? HomeDefaultCollectionCell {
           
            cell.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            
            return cell
        } else{
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
       self.searchBar.resignFirstResponder()
    }
    
}
