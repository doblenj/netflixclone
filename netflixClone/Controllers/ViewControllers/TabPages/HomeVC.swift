//
//  HomeVC.swift
//  netflixClone
//
//  Created by Doble on 28/02/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import KRPullLoader

class HomeVC: UIViewController, KRPullLoadViewDelegate {
    
    //Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var kidsBtn: UIButton!
    @IBOutlet weak var filmsBtn: UIButton!
    @IBOutlet weak var seriesBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var selectedBtnNAmeLbl: UILabel!
    @IBOutlet var mainView: UIView!
    
    //Variables
    var bannerData = [videoImg]
    var currentPageType: String = PageType.home.rawValue
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set home button content mode as aspectfit
        homeBtn.imageView?.contentMode = .scaleAspectFit
        homeBtn.imageView?.clipsToBounds = true
        
        // Register tableview nib
        homeTableView.register(UINib(nibName: Constants.Cells.homeBannerTCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.homeBannerTCell)
        homeTableView.register(UINib(nibName: Constants.Cells.homeCommonCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.homeCommonCell)
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        let loadMoreView = KRPullLoadView()
        refreshView.delegate = self
        homeTableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        homeTableView.addPullLoadableView(loadMoreView, type: .loadMore)
        setUpColor()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.selectedBtnNAmeLbl.isHidden = true
    }
    
    //MARK: Button actions
    @IBAction func homeBtnTapped(_ sender: Any) {
        
        self.seriesBtn.isHidden = false
        self.filmsBtn.isHidden = false
        self.kidsBtn.isHidden = false
        self.selectedBtnNAmeLbl.isHidden = true
        currentPageType = PageType.home.rawValue
    }
    
    @IBAction func seriesBtnTapped(_ sender: Any) {
        
        self.seriesBtn.isHidden = true
        self.filmsBtn.isHidden = true
        self.kidsBtn.isHidden = true
        self.selectedBtnNAmeLbl.isHidden = false
        self.selectedBtnNAmeLbl.text = StringConstants.series
        currentPageType = PageType.series.rawValue
    }
    
    @IBAction func filmsBtnTapped(_ sender: Any) {
        
        self.seriesBtn.isHidden = true
        self.filmsBtn.isHidden = true
        self.kidsBtn.isHidden = true
        self.selectedBtnNAmeLbl.isHidden = false
        self.selectedBtnNAmeLbl.text = StringConstants.films
        currentPageType = PageType.films.rawValue
    }
    
    @IBAction func kidsBtnTapped(_ sender: Any) {
        
        self.seriesBtn.isHidden = true
        self.filmsBtn.isHidden = true
        self.kidsBtn.isHidden = true
        self.selectedBtnNAmeLbl.isHidden = false
        self.selectedBtnNAmeLbl.text = StringConstants.kids
        currentPageType = PageType.kids.rawValue
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        self.homeTableView.backgroundColor = Constants.CommonColors.theameDark
        
        //MIX
        self.headerView.backgroundColor = Constants.CommonColors.theameDark
        self.headerView.alpha = 0.75
        
        //LIGHT
        self.selectedBtnNAmeLbl.textColor = Constants.CommonColors.theameLight
        self.kidsBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.filmsBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.seriesBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }
    
}

// MARK: Tableview delegate datasource
extension HomeVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        }else{
            
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {

            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.homeBannerTCell, for: indexPath) as? HomeBannerCell {
                
                cell.datasource = self
                cell.indexPath = indexPath
//                cell.refresh()
                cell.currentVC = self
                cell.selectionStyle = .none
                return cell
            } else {
                
                return UITableViewCell()
            }
        }else{
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.homeCommonCell, for: indexPath) as? HomeCommonCell {
                
                cell.sectionTitleLbl.text = "Section name"
                cell.cellType = .defaultCell
                cell.datasource = self as HomeCommonCellDataSource
                cell.sectionTitle = "title"
                cell.indexPath = indexPath
                cell.currentVC = self
                
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let titleHeight: CGFloat = 60.0
        
        if indexPath.section == 0 {
            
            return UIScreen.main.bounds.height/1.5
        } else {
            
            return 150 + titleHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            self.navigateToSingleVideoPage(adminVideoID: 0)
        }
    }
    
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            
            switch state {
                
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    
                    completionHandler()
                    
                    self.view.makeToast(StringConstants.pending)
                }
            default: break
            }
            return
        }
        
        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }
}

// DATASource for the collection view inside each cell.
extension HomeVC: HomeCommonCellDataSource {
    
    func numberOfItemsInCollection(in cell: UITableViewCell, at indexPath: IndexPath) -> Int {
        
        return 10
    }
}

// DataSource for the banner collection view inside cell.
extension HomeVC: HomeBannerTableCellDataSource {

    func numberofItemsInBannerCollection(in cell: UITableViewCell, at indexPath:IndexPath) -> Int {
        
        return 1
    }
    
}
