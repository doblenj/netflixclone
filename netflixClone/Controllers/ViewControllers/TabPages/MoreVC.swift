//
//  MoreVC.swift
//  netflixClone
//
//  Created by Doble on 28/02/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class MoreVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    let proTableArray = [StringConstants.proTableArray.appSettings,StringConstants.proTableArray.account,StringConstants.proTableArray.notifications,StringConstants.proTableArray.logOut];
    
    //MARK: Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollSubView: UIView!
    @IBOutlet weak var proSubView: UIView!
    @IBOutlet weak var manageProBtn: UIButton!
    @IBOutlet weak var settingsLbl: UILabel!
    
    
    //MARK: Actions
    
    @IBAction func ManageProfileTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.manageProVC) as? ManageProVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.scrollView.backgroundColor = Constants.CommonColors.theameDark
        self.scrollSubView.backgroundColor = Constants.CommonColors.theameDark
        self.proSubView.backgroundColor = Constants.CommonColors.theameDark
        self.collectionView.backgroundColor = Constants.CommonColors.theameDark
        self.tableView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.manageProBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.settingsLbl.textColor = Constants.CommonColors.theameLight
        
    }
    
    
    
    //MARK: CollectionView delegates
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.moreCCell, for: indexPath as IndexPath) as? MoreCCell {
        
            if indexPath.row % 2 == 0 {
                
                cell.configureMoreCellWith(isDefault: true, indexPath: indexPath)
            } else {
                
                cell.configureMoreCellWith(isDefault: false, indexPath: indexPath)
            }
            
            return cell
            
        } else {
            
            return UICollectionViewCell()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         if indexPath.row % 2 == 0 {
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.editProfileVC) as? EditProfileVC {
                
                vc.isNewProfile = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            
            self.navigateToRootVC()
        }
    }
    
    //MARK: Tableview delegate and datasourse func
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return proTableArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.moreCell, for: indexPath as IndexPath) as? MoreCell {
            
            cell.configureMoreCellWith(name: proTableArray[indexPath.row])
            
            cell.selectionStyle = .none
            
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 { //app settings
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.appSettingsVC) as? AppSettingsVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 1 { //account vc
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.accountVC) as? AccountVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else if indexPath.row == 2 { //notification
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.notificationVC) as? NotificationVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else if indexPath.row == 3 { //logging out
            
            let alert = UIAlertController(title: Constants.appName, message: StringConstants.areYouSureToLogout, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
                
                UserDefaults.standard.set(false, forKey: Constants.Keys.isLoggedIn)
                self.performLogout(msg: "", Vc: self, isForcefull: false)
                
            }))
            alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }

    

}
