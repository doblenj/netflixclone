//
//  LaunchVC.swift
//  netflixClone
//
//  Created by Doble on 20/08/17.
//  Copyright © 2017 BTeem. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard
let username = defaults.value(forKey: Constants.Keys.userNameKey)
let useremail = defaults.value(forKey: Constants.Keys.emailIdKey)
let profileImg = "https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzEwMC81NTcvb3JpZ2luYWwvYmFieS1ibGlua2luZy1zaHV0dGVyc3RvY2suanBn"
let videoImg = "https://m.media-amazon.com/images/M/MV5BMTU2NjA1ODgzMF5BMl5BanBnXkFtZTgwMTM2MTI4MjE@._V1_QL50_SY1000_CR0,0,666,1000_AL_.jpg"
let VdoUrl = "https://www.youtube.com/watch?v=eiGdsH1g20k"

class LaunchVC: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5, execute: {
            
            self.checkIfLoggedIn()
        })
    }
    
    
    //To check weather loged in or not
    fileprivate func checkIfLoggedIn() {
        
        if UserDefaults.standard.bool(forKey: Constants.Keys.isLoggedIn) {
            
            //User is logged in - Show Home page
            self.performSegue(withIdentifier: Constants.SegueIds.homeVC, sender: self)
            
        } else {
            
            // User is not logged in - Show login page
            self.performSegue(withIdentifier: Constants.SegueIds.loginVC, sender: self)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

