
//
//  EditProfileVC.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: Variables
    var isNewProfile : Bool = false
    var stringSubPro: String?
    var selectedImage = UIImage()
    
    //MARK: Outlets
    
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var deleteBtnView: UIView!
    
    //MARK: Actions
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        
        self.backBtnTapped()
    }
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        
        if isValidateParameters() {
            
            if isNewProfile {
                
               self.navigationController?.popViewController(animated: true)
            } else {
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func changePicTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: StringConstants.chooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: StringConstants.camera, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.gallery, style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: StringConstants.cancel, style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func deleteSubProTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: Constants.appName, message: StringConstants.sureToDeleteSubProfile , preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.editProfile
        if isNewProfile {
            
            deleteBtnView.isHidden = true
            self.nameTextField.text = nil
            
        } else {
            
            deleteBtnView.isHidden = false
            self.nameTextField.text = "Profile Name"
        }
        
        self.proImg.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true;
    }
    /// To open camera
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            
            let alert  = UIAlertController(title: StringConstants.warning, message: StringConstants.youDontHaveCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.ok, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    /// To open gallery
    func openGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            
            let alert  = UIAlertController(title: StringConstants.warning, message: StringConstants.youDontHavePerissionToAccessGallery, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.ok, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        selectedImage = info[.originalImage] as! UIImage
        
        proImg.image = selectedImage
        proImg.contentMode = .scaleAspectFill
        
        dismiss(animated: true, completion: nil)
    }
    
    //To manage when the user cancels the picker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //to validate the parameters
    
    func isValidateParameters() -> Bool {
        
        if nameTextField.text == "" || nameTextField.text == nil  {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
            return false
            
        } else {
            
            return true
        }
    }
    


}
