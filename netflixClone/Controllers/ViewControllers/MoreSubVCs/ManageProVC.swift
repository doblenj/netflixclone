//
//  ManageProVC.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class ManageProVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    
    //MARK: Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK: Actions
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.manageProfile
        setUpColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.collectionView.backgroundColor = Constants.CommonColors.theameDark
    }
    
    //MARK: CollectionView delegates
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.manageCCell, for: indexPath as IndexPath) as? ManageCCell {
            
            if indexPath.row % 2 == 0 {
                
                cell.configureMoreCellWith(isDefault: true,indexPath: indexPath)
            } else {
                
                cell.configureMoreCellWith(isDefault: false,indexPath: indexPath)
            }
            
            
            return cell
            
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row % 2 == 0 {
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.editProfileVC
                ) as? EditProfileVC {
                
                vc.isNewProfile = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.editProfileVC
                ) as? EditProfileVC {
                
                vc.isNewProfile = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

}
