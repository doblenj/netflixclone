//
//  PaidVideosVC.swift
//  netflixClone
//
//  Created by Doble on 09/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import  KRPullLoader

class PaidVideosVC: UIViewController, KRPullLoadViewDelegate {

    
    //MARK:Variables
    //MARK: Variables
    var titleString : String?
    
    //MARK: Outlets
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var videosListTable: UITableView!
    @IBOutlet weak var nothingFoundLbl: UILabel!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setUpColor()
        // Register tableview Xib
        videosListTable.register(UINib(nibName: Constants.Cells.videosListTCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.videosListTCell)
        videosListTable.reloadData()
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        let loadMoreView = KRPullLoadView()
        refreshView.delegate = self
        videosListTable.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        videosListTable.addPullLoadableView(loadMoreView, type: .loadMore)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.emptyView.isHidden = true
        self.title = titleString ?? ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.videosListTable.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.nothingFoundLbl.textColor = Constants.CommonColors.theameLight
    }
    
}

extension PaidVideosVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.videosListTCell, for: indexPath) as? VideosListCommonCell {
            
            cell.configureMoreCellWith(indexPath: indexPath)
            cell.selectionStyle = .none
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            
            switch state {
                
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    
                    completionHandler()
                    
                    self.view.makeToast(StringConstants.pending)
                }
            default: break
            }
            return
        }
        
        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }
}
