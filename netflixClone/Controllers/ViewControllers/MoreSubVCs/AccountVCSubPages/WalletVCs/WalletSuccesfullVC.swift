
//
//  WalletSuccesfullVC.swift
//  netflixClone
//
//  Created by Doble on 20/05/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import Lottie

class WalletSuccesfullVC: UIViewController {

    //MARK: Variables
    var amount : String? = ""
    var gifAnimation = LOTAnimationView()
    
    //MARK: Outlets
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var amountLbl: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        gifAnimation = LOTAnimationView(name: "trophy")
        self.animationView.addSubview(gifAnimation)
        self.view.bringSubviewToFront(animationView)
        gifAnimation.loopAnimation = true
        gifAnimation.play()
        self.amountLbl.text = self.amount
    }
    
    override func viewWillLayoutSubviews() {
        
        gifAnimation.frame = animationView.bounds
    }
    
}
