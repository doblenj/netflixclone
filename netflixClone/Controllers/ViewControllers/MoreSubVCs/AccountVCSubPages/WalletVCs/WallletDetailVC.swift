//
//  WallletDetailVC.swift
//  netflixClone
//
//  Created by Doble on 20/05/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class WallletDetailVC: UIViewController {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var paidDateLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    //MARK: Actions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLbl.text = "title comes here"
        self.amountLbl.text = "$50"
        self.paidDateLbl.text = "10-10-19"
        self.descriptionLbl.text = "description comes here"
    }
    

}
