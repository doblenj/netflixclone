//
//  MyWalletVC.swift
//  netflixClone
//
//  Created by Doble on 20/05/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class MyWalletVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    //MARK: Variables
    var isNormalAddMoney : Bool = true
    var enteredAmount : String = ""
    var enteredCoupon : String = ""
    var PaymentID1: String?
    
    //MARK: Outlets
    @IBOutlet weak var titleUIVIew: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var totalBalanceLbl: UILabel!
    @IBOutlet weak var transactionsLbl: UILabel!
    @IBOutlet weak var usedAmountLbl: UILabel!
    
    //Add money
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var addMoneyView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var addMoneyTF: UITextField!
    @IBOutlet weak var payPalBn: UIButton!
    @IBOutlet weak var stripeBtn: UIButton!
    @IBOutlet weak var moneyOrVoucherBtn: UIButton!
    @IBOutlet weak var applyCodeBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    //MARK: Actions
    @IBAction func closeAddMoneyBtnTapped(_ sender: Any) {
        
        self.blurView.isHidden = true
        self.addMoneyView.isHidden = true
        self.addMoneyTF.resignFirstResponder()
    }
    
    @IBAction func paypalBtnTapped(_ sender: Any) {
        
        self.blurView.isHidden = true
        self.addMoneyView.isHidden = true
        
        self.delay(bySeconds: 0.5, closure: {
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.walletSuccesfullVC ) as? WalletSuccesfullVC {
                
                vc.amount = "\(Constants.currencyCode)\(self.addMoneyTF.text)"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    @IBAction func stripeBtnTapped(_ sender: Any) {
        
        self.addMoneyTF.resignFirstResponder()
        if self.addMoneyTF.text != nil && self.addMoneyTF.text != "" && Int(addMoneyTF.text ?? "") ?? 0 > 0 {
            
            let msgString = StringConstants.sureToAddToWallet + self.addMoneyTF.text! + "?"
            let alert = UIAlertController(title: Constants.appName, message: msgString, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
                
                self.blurView.isHidden = true
                self.addMoneyView.isHidden = true
                
                self.delay(bySeconds: 0.5, closure: {
                    
                    let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
                    if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.walletSuccesfullVC ) as? WalletSuccesfullVC {
                        
                        vc.amount = "\(Constants.currencyCode)\(self.addMoneyTF.text)"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                })
            }))
            
            alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            
            self.view.makeToast(StringConstants.pleaseEnterAValidAmount)
        }
    }
    
    @IBAction func moneyOrVoucherBtnTapped(_ sender: Any) {
        
        self.isNormalAddMoney = !isNormalAddMoney
        self.setUpPopUp()
    }
    
    @IBAction func applyCodeBtnTapped(_ sender: Any) {
        
        self.addMoneyTF.resignFirstResponder()
        if self.addMoneyTF.text != nil && self.addMoneyTF.text != "" {
            
            self.blurView.isHidden = true
            self.addMoneyView.isHidden = true
            
            self.delay(bySeconds: 0.5, closure: {
                
                let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
                if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.walletSuccesfullVC) as? WalletSuccesfullVC {
                    
                    vc.amount = "$50"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            })
        } else {
            
            self.view.makeToast(StringConstants.pleaseEnterAValidCode)
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.blurView.isHidden = true
        self.addMoneyView.isHidden = true
        self.addMoneyTF.delegate = self
        self.title = StringConstants.AccountArray.myWallet
        self.headerContentView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10, masksToBounds: true)
        
        let accountImage    = UIImage(named: "add")!
        let addButton   = UIBarButtonItem(image: accountImage,  style: .plain, target: self, action: #selector(addButtonTapped))
        navigationItem.rightBarButtonItems = [addButton]
        self.totalBalanceLbl.text = "$100"
        self.usedAmountLbl.text = "$56"
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if isNormalAddMoney {
            
            self.enteredAmount = self.addMoneyTF.text ?? ""
        } else {
            
            self.enteredCoupon = self.addMoneyTF.text ?? ""
        }
    }
    
    func setUpPopUp() {
        
        if isNormalAddMoney {
            
            self.titleLbl.text = StringConstants.addMoney
            self.moneyOrVoucherBtn.setTitle(StringConstants.orVoucher, for: .normal)
            self.stripeBtn.isHidden = false
            self.payPalBn.isHidden = false
            self.applyCodeBtn.isHidden = true
            self.addMoneyTF.text = enteredAmount
            self.addMoneyTF.keyboardType = .numberPad
            self.addMoneyTF.autocapitalizationType = .none
        } else {
            
            self.titleLbl.text = StringConstants.addVoucher
            self.moneyOrVoucherBtn.setTitle(StringConstants.orMoney, for: .normal)
            self.stripeBtn.isHidden = true
            self.payPalBn.isHidden = true
            self.applyCodeBtn.isHidden = false
            self.addMoneyTF.text = enteredCoupon
            self.addMoneyTF.keyboardType = .default
            self.addMoneyTF.autocapitalizationType = .allCharacters
        }
    }
    
    @objc private func addButtonTapped(_ sender: UIButton?) {
        
        self.setUpPopUp()
        self.blurView.isHidden = false
        self.addMoneyView.isHidden = false
    }
    
    
    //MARK: UItableview delegate and datasourse func
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.walletCell, for: indexPath as IndexPath) as? WalletCell {
            
            cell.configureMoreCellWith(indexpath: indexPath)
            
            cell.selectionStyle = .none
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.wallletDetailVC) as? WallletDetailVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
