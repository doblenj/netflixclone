//
//  EditAccountVC.swift
//  netflixClone
//
//  Created by Doble on 12/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class EditAccountVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    //Variables
    var dictLocal = Dictionary<String, Any>()
    
    //Outlets
    @IBOutlet weak var proPicUIView: UIView!
    @IBOutlet weak var proPicImgView: UIImageView!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var btnUIView: UIView!
    @IBOutlet weak var proChangeBtn: UIButton!
    
    //Actions
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        
        defaults.set(self.nameTextField.text, forKey: Constants.Keys.userNameKey)
        defaults.set(self.phoneTextField.text, forKey: Constants.Keys.phoneNumKey)
        defaults.set(self.emailTextField.text, forKey: Constants.Keys.emailIdKey)
        defaults.synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectImgBtnTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: StringConstants.chooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: StringConstants.camera, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.gallery, style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: StringConstants.cancel, style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.editAccount
        
        self.nameTextField.text = UserDefaults.standard.value(forKey: Constants.Keys.userNameKey) as? String
        self.emailTextField.text = UserDefaults.standard.value(forKey: Constants.Keys.emailIdKey) as? String
        self.phoneTextField.text = UserDefaults.standard.value(forKey: Constants.Keys.phoneNumKey) as? String
        self.proPicImgView.sd_setImage(with: URL(string: UserDefaults.standard.value(forKey: Constants.Keys.userPicKey) as? String ?? ""), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
        
        self.btnUIView.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
        self.proPicUIView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: proPicUIView.frame.width/2, masksToBounds: true)
    }
    
    /// To open camera
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            
            let alert  = UIAlertController(title: StringConstants.warning, message: StringConstants.youDontHaveCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.ok, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    /// To open gallery
    func openGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            
            let alert  = UIAlertController(title: StringConstants.warning, message: StringConstants.youDontHavePerissionToAccessGallery, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstants.ok, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            
            print("Error: \(info)")
            return
        }
        
        proPicImgView.image = selectedImage
        proPicImgView.contentMode = .scaleAspectFill
        
        dismiss(animated: true, completion: nil)
    }
    
    //To manage when the user cancels the picker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func callService(service: String, params: [String: Any], key: String) {
        
        AlamofireHC.requestPOSTwithImage(service, image: proPicImgView.image!, params: params as [String : AnyObject], imageParam: "picture", headers: nil, success: { (response) in
            
            debugPrint(response)
            let  result = response.dictionaryObject
            let resultcheck = result!["success"] as! Bool
            
            if(resultcheck){
                let msg = result!["message"] as! String
                let data = result!["data"] as! NSDictionary
                let defaults = UserDefaults.standard
                let name = data["name"] as! String
                let email = data["email"] as! String
                let mobile = data["mobile"] as! String
                let pic =  data["picture"] as! String
                
                defaults.set(name, forKey: Constants.Keys.userNameKey)
                defaults.set(pic, forKey: Constants.Keys.userPicKey)
                defaults.set(mobile, forKey: Constants.Keys.phoneNumKey)
                defaults.set(email, forKey: Constants.Keys.emailIdKey)
                defaults.synchronize()
                self.view.makeToast(msg)
                DispatchQueue.main.async {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            } else {
                
                let errorCode: Int = result!["error_code"] as? Int ?? 0
                let msg = result!["error"] as? String ?? ""
                
                if ValidationClass.shouldForceLogoutForErrorCode(errorCode: errorCode) {
                    
                    self.performLogout(Vc: self)
                } else {
                    
                    self.view.makeToast(msg)
                }
            }
            
        }) { (error) in
            debugPrint(error)
            self.view.makeToast(StringConstants.pleaseTryAgain)
        }
    }
    
    //to validate the parameters
    
    func isValidateParameters() -> Bool {
        
        if emailTextField.text == "" || emailTextField.text == nil || nameTextField.text == "" || nameTextField.text == nil  {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
            return false
            
        } else {
            
            if ValidationClass.isValidEmailID(email: self.emailTextField.text ?? "") {
                
                return true
            } else {
                
                self.view.makeToast(StringConstants.pleaseGiveAValidEmailAddress)
                return false
            }
        }
    }
    

}
