//
//  CardsVC.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import KRPullLoader

class CardsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, KRPullLoadViewDelegate {
    
    //MARK: Variables
    var defaultCard = Int()
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCardBtn: UIButton!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var addCardUIView: UIView!
    
    
    //Actions
    @IBAction func addCardBtnTapped(_ sender: Any) {
        
        self.view.makeToast(StringConstants.pending)
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.cards
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        self.addCardUIView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
    }
    
    
    //MARK: UItableview delegate and datasourse func

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.cardsCell, for: indexPath as IndexPath) as? CardsCell {
            
            if indexPath.row == defaultCard {
                
                cell.configureMoreCellWith(isDefault: true, indexPath: indexPath)
            } else {
                
                cell.configureMoreCellWith(isDefault: false, indexPath: indexPath)
            }
            
            cell.selectionStyle = .none
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != defaultCard {
            
            self.defaultCard = indexPath.row
            self.tableView.reloadData()
        }
    }
    
    //to delete the row
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
            self.view.makeToast("Delete card Pending")
        }
    }
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {

        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }
    
    
}
