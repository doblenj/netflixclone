//
//  MyPlanDetailsVC.swift
//  netflixClone
//
//  Created by Doble on 13/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class MyPlanDetailsVC: UIViewController {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var nameOfPlans: UILabel!
    @IBOutlet weak var numOfAccValue: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var perMonthLbl: UILabel!
    @IBOutlet weak var expiryLbl: UILabel!
    @IBOutlet weak var paymentModeLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var paymentIDLbl: UILabel!
    
    //MARK: Actions

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.nameOfPlans.text = "basic plan"
        self.numOfAccValue.text = "2"
        self.amountLbl.text = "$50"
        self.perMonthLbl.text = "2 Month(s)"
        self.descriptionLbl.text = "This is a basic plan"
        self.expiryLbl.text = "10-10-19"
        self.paymentModeLbl.text = "Card"
        self.totalAmountLbl.text = "$50"
        self.paymentIDLbl.text = "dg55dfdg5whww77wbf"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        if GeneralSingleton.shared.isFromPlansSuccssfullPayment {
            
            GeneralSingleton.shared.isFromPlansSuccssfullPayment = false
            
            let viewControllers = self.navigationController!.viewControllers as [UIViewController]
            for aViewController:UIViewController in viewControllers {
                
                if aViewController.isKind(of: AccountVC.self) {
                    
                    _ = self.navigationController?.popToViewController(aViewController, animated: false)
                }
            }
        }
    }

}
