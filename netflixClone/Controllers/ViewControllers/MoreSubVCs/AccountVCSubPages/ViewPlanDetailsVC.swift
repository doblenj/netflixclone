//
//  ViewPlanDetailsVC.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class ViewPlanDetailsVC: UIViewController {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var nameOfPlans: UILabel!
    @IBOutlet weak var numOfAccValue: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var perMonthLbl: UILabel!
    
    //MARK: Actions
    
    @IBAction func payBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.paymentsVC) as? PaymentsVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        payBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        
        self.nameOfPlans.text = "Basic plan"
        self.numOfAccValue.text = "3"
        self.amountLbl.text = "\(Constants.currencyCode)50"
        self.perMonthLbl.text = "3"
        self.descriptionLbl.text = "Lorem ipsom"
        payBtn.setTitle(StringConstants.select,for: .normal)
    }
    

}
