//
//  MyPlansVC.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import KRPullLoader

class MyPlansVC: UIViewController, UITableViewDelegate, UITableViewDataSource, KRPullLoadViewDelegate {

    
    //MARK: Variables
    var goingToDetailsFromInvoice : Bool = false
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var autoRenewalSwitch: UISwitch!
    @IBOutlet weak var autoRenewalView: UIView!
    @IBOutlet weak var emptyView: UIView!
    
    //Actions
    @IBAction func switchTapped(_ sender: Any) {
        
        var msg : String?
        
        if autoRenewalSwitch.isOn {
            
            msg = StringConstants.enableAutoRenewal
        } else {
            
            msg = StringConstants.disableAutoRenewal
        }
        
        let alert = UIAlertController(title: Constants.appName, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
            
            if self.autoRenewalSwitch.isOn {
            
                self.autoRenewalSwitch.isOn = false
            } else {
                
               self.autoRenewalSwitch.isOn = true
            }
            
        }))
        alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.AccountArray.myPlans
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        let loadMoreView = KRPullLoadView()
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        if !goingToDetailsFromInvoice {
            
            if GeneralSingleton.shared.isFromPlansSuccssfullPayment {
                
                GeneralSingleton.shared.isFromPlansSuccssfullPayment = false
                
                if self.navigationController?.viewControllers.count ?? 0 > 0 {
                    
                    let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                    if viewControllers.count > 0 {
                        
                        for aViewController:UIViewController in viewControllers {
                            
                            if aViewController.isKind(of: AccountVC.self) {
                                
                                _ = self.navigationController?.popToViewController(aViewController, animated: false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    //MARK: UItableview delegate and datasourse func
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.myPlansCell, for: indexPath as IndexPath) as? MyPlansCell {
            
            cell.configureMoreCellWith(indexPath: indexPath)
            
            cell.selectionStyle = .none
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.myPlanDetailsVC) as? MyPlanDetailsVC {
            
            self.goingToDetailsFromInvoice = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            
            switch state {
                
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    
                    completionHandler()
                    
                    self.view.makeToast(StringConstants.pending)
                }
            default: break
            }
            return
        }
        
        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }

}
