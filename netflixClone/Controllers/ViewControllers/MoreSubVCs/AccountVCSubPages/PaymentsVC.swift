//
//  PaymentsVC.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PaymentsVC: UIViewController {

    
    //MARK: Variables
    var isFromPlansSuccssfullPayment : Bool = true
    var totalAmount = 50
    var remainingAmount = 30
    var couponAmount = 20
    var couponCode : String? = ""
    var PaymentID1: String?
    
    
    //MARK: Outlets
    @IBOutlet weak var planTitleName: UILabel!
    @IBOutlet weak var numOfMonths: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var amountToPayLbl: UILabel!
    @IBOutlet weak var couponTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var payUIView: UIView!
    @IBOutlet weak var payThroughUIView: UIView!
    @IBOutlet weak var payPalBtn: UIButton!
    @IBOutlet weak var stripeBtn: UIButton!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var couponCodeLbl: UILabel!
    @IBOutlet weak var couponAmountLbl: UILabel!
    @IBOutlet weak var couponRemoveBtn: UIButton!
    @IBOutlet weak var couponSuccessView: UIView!
    @IBOutlet weak var totalAmountSuccessLbl: UILabel!
    @IBOutlet weak var toPaySuccessLbl: UILabel!
    @IBOutlet weak var monthsTitleLbl: UILabel!
    @IBOutlet weak var appliedCoupponCode: UILabel!
    
    //MARK: Actions
    
    @IBAction func couponRemoveBtnTapped(_ sender: Any) {
        
        self.couponSuccessView.isHidden = true
        self.remainingAmount = 50
        self.couponTextField.text = ""
        self.couponAmount = 0
        
        self.monthsTitleLbl.isHidden = false
        self.numOfMonths.isHidden = false
        
        payThroughUIView.isHidden = false
        payUIView.isHidden = true
    }
    
    
    @IBAction func couponApplyTapped(_ sender: Any) {
        
        if couponTextField.text != nil && couponTextField.text != "" {
            
            self.couponSuccessView.isHidden = false
            self.remainingAmount = 30
            let codeApplied = self.couponTextField.text
            self.couponAmount = 20
                
            self.totalAmountSuccessLbl.text = "$50"
            self.toPaySuccessLbl.text = "$30"
            self.couponAmountLbl.text = "$20"
            self.couponCodeLbl.text = codeApplied
            self.appliedCoupponCode.text = codeApplied
            
            if self.remainingAmount > 0 {
                
                self.payThroughUIView.isHidden = false
                self.payUIView.isHidden = true
            } else {
                
                self.payThroughUIView.isHidden = true
                self.payUIView.isHidden = false
            }
            
        } else {
            
            self.view.makeToast(StringConstants.enterCode)
        }
    }
    
    @IBAction func payBtnTapped(_ sender: Any) {
        
        self.pushToInvoiceVC()
    }
    
    @IBAction func paypalBtnTapped(_ sender: Any) {
        
        self.pushToInvoiceVC()
    }
    
    
    func pushToInvoiceVC () {
        
        let msgString = "\(StringConstants.sureToPayAmount) \(self.remainingAmount)?)"
        let alert = UIAlertController(title: Constants.appName, message: msgString, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
            
            if self.couponSuccessView.isHidden {
                
                self.remainingAmount = 50
                self.couponAmount = 0
                
                self.couponCode = ""
            } else {
                
                self.remainingAmount = 30
                self.couponAmount = 20
                self.couponCode = self.couponTextField.text
            }
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.invoiceVC) as? InvoiceVC {
                
                vc.couponAmount = self.couponAmount
                vc.paidAmount = self.remainingAmount
                vc.totalAmount = self.totalAmount
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func stripeBtnTapped(_ sender: Any) {
        
        self.pushToInvoiceVC()

    }
    
    @IBAction func walletBtnTapped(_ sender: Any) {
        
        self.pushToInvoiceVC()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.payments
        self.couponSuccessView.isHidden = true
        payThroughUIView.isHidden = true
        
        self.monthsTitleLbl.isHidden = false
        self.numOfMonths.isHidden = false
        
        
        applyBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        payBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        
        self.planTitleName.text = "Basic plan"
        self.numOfMonths.text = "3 Month(s)"
        self.totalAmountLbl.text = "\(Constants.currencyCode)\(self.totalAmount)"
        self.amountToPayLbl.text = "\(Constants.currencyCode)\(self.remainingAmount)"
        
        if self.remainingAmount > 0 {
            
            payThroughUIView.isHidden = false
            payUIView.isHidden = true
        } else {
            
            payThroughUIView.isHidden = true
            payUIView.isHidden = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        if GeneralSingleton.shared.isFromPlansSuccssfullPayment {
            
            GeneralSingleton.shared.isFromPlansSuccssfullPayment = false
            
            if self.navigationController?.viewControllers.count ?? 0 > 0 {
                
                let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                for aViewController:UIViewController in viewControllers {
                    
                    if aViewController.isKind(of: AccountVC.self) {
                        
                        _ = self.navigationController?.popToViewController(aViewController, animated: false)
                    }
                }
            }
        }
    }
    
}
