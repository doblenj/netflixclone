//
//  InvoiceVC.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import Lottie

class InvoiceVC: UIViewController {

    //MARK: Variables
    var gifAnimation = LOTAnimationView()
    var isPlanPayment : Bool = true // this page can come from both view plans and single video page
    var couponAmount = 20
    var paidAmount = 30
    var totalAmount = 50
    
    //MARK: Outlets
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var headingsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var paymentIdLbl: UILabel!
    @IBOutlet weak var paidAmountLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var myPlanBtn: UIButton!
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var couponAmountPaidLbl: UILabel!
    
    //MARK: Actions
    @IBAction func myPlanBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.myPlansVC) as? MyPlansVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func dismissBtnTapped(_ sender: Any) {
        
        backBtnTapped()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.invoice
        GeneralSingleton.shared.isFromPlansSuccssfullPayment = true

        myPlanBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        dismissBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        gifAnimation = LOTAnimationView(name: "success")
        self.animationView.addSubview(gifAnimation)
        self.view.bringSubviewToFront(animationView)
        gifAnimation.play{ (finished) in
            // Do Something
        }
        
        self.headingsLbl.text = "Payment Success"
        self.titleLbl.text = "title comes here"
        self.paymentIdLbl.text = "fdjj7ddf67832bd"
        self.totalAmountLbl.text = "$\(self.totalAmount)"
        self.paidAmountLbl.text = "$\(self.paidAmount)"
        self.couponAmountPaidLbl.text = "$\(self.couponAmount)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.myPlanBtn.isHidden = false
    }
        
    override func viewWillLayoutSubviews() {
        
        gifAnimation.frame = animationView.bounds
    }
    

}
