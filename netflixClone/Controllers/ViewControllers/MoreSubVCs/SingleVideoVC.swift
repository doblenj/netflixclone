//
//  SingleVideoVC.swift
//  netflixClone
//
//  Created by Doble on 11/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class SingleVideoVC: UIViewController {
    
    
    // Outlets
    @IBOutlet weak var singleVideoTable: UITableView!
    @IBOutlet weak var pickerBackView: UIView!
    @IBOutlet weak var seasonPickerView: UIPickerView!
    @IBOutlet weak var payOptionsView: UIView!
    @IBOutlet weak var firstHeadingLbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondHeadingLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var blurView: UIView!
    
    //Variables
    var videoID: Int? = 0
    var params = [String: Any]()
    var isSeasonVideoSelected: Bool = true
    var pickerData: [String] = [String]()
    var seasonId : Int?
    
    //Actions:
    
    @IBAction func viewPlanPayBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.viewPlansVC
            ) as? ViewPlansVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func ppvPayBtnTapped(_ sender: Any) {
        
        //show ppv page to user
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.paymentsVC) as? PaymentsVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func closePayOptionBtnTapped(_ sender: Any) {

        self.payOptionsView.isHidden = true
        self.blurView.isHidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpColor()
        // Register header view for season and trailer cell
        singleVideoTable.register(UINib(nibName: Constants.ViewXibs.singleVideoHeaderView, bundle: nil), forHeaderFooterViewReuseIdentifier: Constants.ViewXibs.singleVideoHeaderView)
        
        // Register xib for Genre and trailer sections
        singleVideoTable.register(UINib(nibName: Constants.Cells.seasonTableCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.seasonTableCell)
        singleVideoTable.register(UINib(nibName: Constants.Cells.trailerTableCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.trailerTableCell)
        singleVideoTable.register(UINib(nibName: Constants.Cells.moreLikeThisTableCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.moreLikeThisTableCell)
        
        // Initially hide the pickerview
        
        self.pickerBackView.isHidden = true
        UIApplication.shared.keyWindow?.bringSubviewToFront(self.pickerBackView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        self.payOptionsView.isHidden = true
        self.firstBtn.setBorderProperties(borderColor: Constants.CommonColors.theameDark, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        self.secondBtn.setBorderProperties(borderColor: Constants.CommonColors.theameDark, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        self.payOptionsView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        self.blurView.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
//        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.singleVideoTable.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
//        self.titleL.textColor = Constants.CommonColors.theameLight
//        self.clearAllBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }
    
    
    //MARK: Button action from section's header view
    // Season tapped : default
    @objc func seasonBtnTapped(sender: UIButton) {
        
        isSeasonVideoSelected = true
        
        // reload last section
        singleVideoTable.reloadSections(IndexSet(integer: 3), with: .none)
    }
    
    // Trailer tapped
    @objc func trailerBtnTapped(sender: UIButton) {
        
        isSeasonVideoSelected = false
        
        // reload last section
        singleVideoTable.reloadSections(IndexSet(integer: 3), with: .none)
    }
    
    @objc func seasonListBtnTapped(sender: UIButton){
        
        self.pickerBackView.isHidden = false
    }

}

extension SingleVideoVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        /*
         1.Player
         2.Detailed
         3.Utility
         4.Trailer and Genre
         */
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 3 {
            
            if isSeasonVideoSelected {
                
                return 5
                
            } else {
                
                return 5
            }
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            return UIScreen.main.bounds.height/2.5
            
        } else if indexPath.section == 3 {
            
            if !isSeasonVideoSelected{
                
                return 200
                
            }else{
                
                
                // Total height calculation for the collection view without scrolling and set it to the tableview cell height - using video count
                let videosCount = 5
                var cellHeight: Int = 0
                if videosCount % 3 == 0 {
                    cellHeight = Int(videosCount/3 * 180)
                }else{
                    cellHeight = Int((videosCount/3 + 1) * 180)
                }
                
                return CGFloat(cellHeight)
                
            }
        }else{
            
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 3 {
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.ViewXibs.singleVideoHeaderView) as! SingleVideoHeaderView
            
            headerView.seasonBtn.addTarget(self, action: #selector(seasonBtnTapped(sender:)), for: .touchUpInside)
            headerView.trailersBtn.addTarget(self, action: #selector(trailerBtnTapped(sender:)), for: .touchUpInside)
            headerView.trailersBtn.setTitle(StringConstants.trailersAndMore, for: .normal)
            
            headerView.seasonBtn.setTitle( StringConstants.moreLikeThese, for: .normal)
            
            // Set border view color
            if isSeasonVideoSelected {
                
                headerView.seasonBorderView.backgroundColor = UIColor.red
                headerView.trailerBorderView.backgroundColor = UIColor.clear
            }else{
                
                headerView.seasonBorderView.backgroundColor = UIColor.clear
                headerView.trailerBorderView.backgroundColor = UIColor.red
            }
            return headerView
            
        }else{
            
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 3 {
            
            if !isSeasonVideoSelected{
                
                return 60
            }else {
                
                return 115
            }
        }else{
            
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.playerTableCell) as? PlayerTableCell {
                
                cell.currentVC = self
                cell.updateUI()
                cell.selectionStyle = .none
                return cell
            } else {
                
                return UITableViewCell()
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.detailedTableCell) as? DetailedTableCell {
                
                cell.currentVC = self
                cell.indexPath = indexPath
                cell.updateUI()
                cell.selectionStyle = .none
                return cell
            } else {
                
                return UITableViewCell()
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.utilityTableCell) as? UtilityTableCell {
                
                cell.currentVC = self
                cell.updateUI()
                cell.selectionStyle = .none
                return cell
            } else {
                
                return UITableViewCell()
            }
        case 3:

            if isSeasonVideoSelected {
                
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.moreLikeThisTableCell) as? MoreLikeThisTableCell {
                
                    cell.currentVC = self
                    cell.selectionStyle = .none
                    return cell
                } else {
                    
                    return UITableViewCell()
                }
                
            }else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.trailerTableCell) as? TrailersTableCell {
                
                    cell.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
                    cell.selectionStyle = .none
                    return cell
                } else {
                    
                    return UITableViewCell()
                }
            }

        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 3 {
            
            if isSeasonVideoSelected {
                
                self.videoID = 0
                self.viewWillAppear(false)
            } else {
               
                let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
                
                if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.playerVC) as? PlayerVC {
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension SingleVideoVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        DispatchQueue.main.async {
            
            self.pickerBackView.isHidden = true
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {

        let seasonTitle = NSAttributedString(string: pickerData[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!])

        return seasonTitle
    }
}
