//
//  CategoryDetailedVC.swift
//  netflixClone
//
//  Created by Doble on 14/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryDetailedVC: UIViewController {

    // Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var navigationUIView: UIView!
    
    //Variables
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpColor()
        
        // Register tableview nib
        categoryTableView.register(UINib(nibName: Constants.Cells.homeBannerTCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.homeBannerTCell)
        categoryTableView.register(UINib(nibName: Constants.Cells.homeCommonCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.homeCommonCell)
        
        self.titleLbl.text = "Category name"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    

    //MARK: back button action
    @IBAction func backBtnTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.navigationUIView.backgroundColor = Constants.CommonColors.theameDark
       
        self.categoryTableView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.titleLbl.textColor = Constants.CommonColors.theameLight
        self.titleLbl.textColor = Constants.CommonColors.theameLight
       
    }
}

// MARK: Tableview delegate datasource
extension CategoryDetailedVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        }else{
            
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.homeBannerTCell, for: indexPath) as? HomeBannerCell {
                
                cell.datasource = self as HomeBannerTableCellDataSource
                cell.indexPath = indexPath
//                cell.refresh()
                cell.currentVC = self
                cell.selectionStyle = .none
                return cell
            } else {
                
                return UITableViewCell()
            }
        }else{
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.homeCommonCell, for: indexPath) as? HomeCommonCell {
                
                cell.sectionTitleLbl.text = "Section title"
                cell.cellType = .defaultCell
                cell.datasource = self as HomeCommonCellDataSource
                cell.sectionTitle = "Section title"
                cell.indexPath = indexPath
                cell.currentVC = self
//                cell.refresh()
                return cell
            } else {
                
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let titleHeight: CGFloat = 60.0
        
        if indexPath.section == 0 {
            
            return UIScreen.main.bounds.height/1.5
        }else {
            
            return 150 + titleHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            self.navigateToSingleVideoPage(adminVideoID: 0)
        }
    }
}

// DATASource for the collection view inside each cell.
extension CategoryDetailedVC: HomeCommonCellDataSource {
    
    func numberOfItemsInCollection(in cell: UITableViewCell, at indexPath: IndexPath) -> Int {
        
        return 10
    }
}

// DataSource for the banner collection view inside cell.
extension CategoryDetailedVC: HomeBannerTableCellDataSource {
    
    func numberofItemsInBannerCollection(in cell: UITableViewCell, at indexPath:IndexPath) -> Int {
        
        return 1
    }
    
}
