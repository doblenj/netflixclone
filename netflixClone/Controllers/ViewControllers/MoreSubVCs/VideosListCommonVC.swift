//
//  VideosListCommonVC.swift
//  netflixClone
//
//  Created by Doble on 09/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import KRPullLoader

class VideosListCommonVC: UIViewController, KRPullLoadViewDelegate {

    //MARK: Variables
    var titleString : String?
    var longPressRecognizer = UILongPressGestureRecognizer()
    
    //MARK: Outlets
    @IBOutlet weak var videosListTable: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var clearAllBtnUIView: UIView!
    @IBOutlet weak var nothingFoundLbl: UILabel!
    @IBOutlet weak var clearAllBtn: UIButton!
    
    //MARK: Actions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpColor()
        
        longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        
        self.view.addGestureRecognizer(longPressRecognizer)
        
        // Register tableview Xib
        videosListTable.register(UINib(nibName: Constants.Cells.videosListTCell, bundle: nil), forCellReuseIdentifier: Constants.Cells.videosListTCell)
        videosListTable.reloadData()
        
        //MARK: setup refresh controllers
        let refreshView = KRPullLoadView()
        let loadMoreView = KRPullLoadView()
        refreshView.delegate = self
        videosListTable.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        videosListTable.addPullLoadableView(loadMoreView, type: .loadMore)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.emptyView.isHidden = true
        self.title = titleString ?? ""
        self.clearAllBtnUIView.isHidden = true
        self.view.bringSubviewToFront(self.clearAllBtnUIView)
        self.view.bringSubviewToFront(self.self.emptyView)
        clearAllBtnUIView.setBorderProperties(borderColor: Constants.CommonColors.theameLight, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
        
        if titleString != StringConstants.AccountArray.wishList && titleString != StringConstants.AccountArray.spamVideos && titleString != StringConstants.AccountArray.history {
            
           self.clearAllBtnUIView.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func clearAllTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: Constants.appName, message: StringConstants.sureToClearAll, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
            
            self.view.makeToast(StringConstants.pending)
        }))
        alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.videosListTable.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.nothingFoundLbl.textColor = Constants.CommonColors.theameLight
        self.clearAllBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        
        if sender.state == UIGestureRecognizer.State.began {
            
            let touchPoint = sender.location(in: self.videosListTable)
            if let indexPath = videosListTable.indexPathForRow(at: touchPoint) {
                
                let alert = UIAlertController(title: Constants.appName, message: StringConstants.sureToDeleteThisVideo, preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
                    
                    self.view.makeToast(StringConstants.pending)
                }))
                alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
}

extension VideosListCommonVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.videosListTCell, for: indexPath) as? VideosListCommonCell {
        
            cell.configureMoreCellWith(indexPath: indexPath)
            cell.selectionStyle = .none
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if titleString == StringConstants.AccountArray.spamVideos {
            
            self.view.makeToast(StringConstants.removeFromSpamToPlay)
        } else  {
            
            self.navigateToSingleVideoPage(adminVideoID: 0)
        }
    }
    
    //MARK: To manage the refresh and load more
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            
            switch state {
                
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    
                    completionHandler()
                    
                    self.view.makeToast(StringConstants.pending)
                }
            default: break
            }
            return
        }
        
        switch state {
            
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                
                pullLoadView.messageLabel.text = ""
            } else {
                
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                
                completionHandler()
                
                self.view.makeToast(StringConstants.pending)
            }
        }
    }
    
}
