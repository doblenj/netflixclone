//
//  StaticPagesVC.swift
//  Rental User
//
//  Created by Doble on 18/01/19.
//  Copyright © 2019 Codegama. All rights reserved.
//

import UIKit

class StaticPagesVC: UIViewController {

    //MARK: Variables
    var heading : String? = ""
    
    //MARK: Outlets
    @IBOutlet weak var textView: UITextView!
    
    
    //MARK: Actions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = heading
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.textView.text = "The static page datas like terms, policy etc comes here"
    }
    
    
    
    
}
