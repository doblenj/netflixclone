//
//  AppSettingsVC.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class AppSettingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK: Variables
    
    let notiIconArray = ["notificationIcon","notificationIcon"];
    let notStatusIconArray = ["switchOff","switchOn"]
    let legalIconArray = ["legalIcon","legalIcon"];
    let notiArray = [StringConstants.notiArray.emailNotifications,StringConstants.notiArray.pushNotifications]
    let legalArray = [StringConstants.legalArray.privacyPolicy, StringConstants.legalArray.termsOfUse]
    let sectionArray = [StringConstants.sectionArray.notifications, StringConstants.sectionArray.legal]
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Actions
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.appSettings
        self.tableView.sectionHeaderHeight = 50
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: tableview delegate functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section) {
        case 0:
            return notiArray.count
        case 1:
            return legalArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = sectionArray[section]
        label.font = .systemFont(ofSize: 20)
        label.textColor = UIColor.white
        headerView.backgroundColor = UIColor.black
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        switch (indexPath.section) {
        case 0:
        
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.appSettingsCell, for: indexPath as IndexPath) as? AppSettingsCell {
                
                var status : Int? = 0
                
                if indexPath.row == 0 {
                    
                    status = UserDefaults.standard.value(forKey: Constants.Keys.emailNotiStatus) as? Int
                    
                } else if indexPath.row == 1 {
                    
                    status = UserDefaults.standard.value(forKey: Constants.Keys.pushNotiStatus) as? Int
                }
                
                cell.configureMoreCellWith(name: notiArray[indexPath.row], iconImg: notiIconArray[indexPath.row], notiImg: notStatusIconArray[status!], indexPath: indexPath)
                cell.notiImg.isHidden = false
                cell.selectionStyle = .none
                return cell
                
            } else {
                
                return UITableViewCell()
            }
            
        case 1:
        
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.appSettingsCell, for: indexPath as IndexPath) as? AppSettingsCell {
                
                cell.configureMoreCellWith(name: legalArray[indexPath.row], iconImg: legalIconArray[indexPath.row], notiImg: "", indexPath: indexPath)
                cell.notiImg.isHidden = true
                cell.selectionStyle = .none
                return cell
                
            } else {
                
                return UITableViewCell()
            }
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (indexPath.section) {
        case 0:
            
            if indexPath.row == 0 {
                
                var status : Int = 0
                
                if defaults.value(forKey: Constants.Keys.emailNotiStatus) as! Int == 1 {
                    
                    status = 0
                } else if defaults.value(forKey: Constants.Keys.emailNotiStatus) as! Int == 0 {
                    status = 1
                }
                
                defaults.set(status, forKey: Constants.Keys.emailNotiStatus)
                self.tableView.reloadData()
                
            } else if indexPath.row == 1 {
                
                var status : Int = 0
                
                if defaults.value(forKey: Constants.Keys.pushNotiStatus) as! Int == 1 {
                    
                    status = 0
                } else if defaults.value(forKey: Constants.Keys.pushNotiStatus) as! Int == 0 {
                    status = 1
                }
                
                defaults.set(status, forKey: Constants.Keys.pushNotiStatus)
                self.tableView.reloadData()
            }
            
        case 1:
            if indexPath.row == 0 {
                
                let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
                
                if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.staticPagesVC) as? StaticPagesVC {
                    
                    vc.heading = legalArray[indexPath.row]
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else if indexPath.row == 1 {
                
                let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
                
                if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.staticPagesVC) as? StaticPagesVC {
                    
                    vc.heading = legalArray[indexPath.row]
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        default:
            return
        }
    }

}
