//
//  PlayerVC.swift
//  netflixClone
//
//  Created by Doble on 19/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import AVKit

class PlayerVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    
    // Variables
    var yTPlayerView = YTPlayerView()
    var appDelegate = AppDelegate()
    var avPlayer = AVPlayer()
    var avPlayerController = AVPlayerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Notification for app inactive
        NotificationCenter.default.addObserver(self, selector: #selector(appInactive(notification:)), name: UIApplication.willResignActiveNotification, object: nil)
        
        // Notification for application become active
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        // Notification for video did end
        NotificationCenter.default.addObserver(self, selector: #selector(videoEndNotification(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        self.view.bringSubviewToFront(backBtn)
        self.updatePlayer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true
        UIDevice.current.setValue(Int(UIInterfaceOrientation.landscapeRight.rawValue), forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        yTPlayerView.removeFromSuperview()
    }
    
    override func viewWillLayoutSubviews() {
        
        yTPlayerView.frame = playerView.bounds
        avPlayerController.view.frame = playerView.bounds
        self.view.bringSubviewToFront(backBtn)
    }
    
    //MARK: Player
    func updatePlayer() {
        
        let videoUrlStr = VdoUrl
        
        playerView.addSubview(yTPlayerView)
        yTPlayerView.delegate = self
        
        let playerParams = ["controls": 2,
                            "autoplay": 1,
                            "playsinline": 1,
                            "autohide": 1,
                            "theme": "light",
                            "color": "red"
            ] as [String : Any]

        let getVideoID = videoUrlStr.components(separatedBy: "=")
        let videoID = getVideoID.last ?? ""
        
        yTPlayerView.load(withVideoId: videoID, playerVars: playerParams)
        
        yTPlayerView.autoresizingMask = [.flexibleBottomMargin,
                                         .flexibleWidth,
                                         .flexibleHeight,
                                         .flexibleLeftMargin,
                                         .flexibleRightMargin,
                                         .flexibleTopMargin]

    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.avPlayer.pause()
            self.avPlayerController.removeFromParent()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: Notification call back
    
    @objc func appInactive(notification: Notification){
        
        DispatchQueue.main.async {
            
            self.appDelegate.shouldRotate = true
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight, forKey: "orientation")
        }
    }
    
    @objc func appBecomeActive(notification: Notification){
        
        DispatchQueue.main.async {
            
            self.avPlayer.pause()
            self.appDelegate.shouldRotate = true
            if UIDevice.current.orientation == .landscapeRight{
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            }
        }
    }
    
    @objc func videoEndNotification(notification: Notification){
        
        self.avPlayer.pause()
        self.avPlayerController.removeFromParent()
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        
        return false
    }
    
    
}

extension PlayerVC: YTPlayerViewDelegate{
    
}

extension PlayerVC: AVPlayerViewControllerDelegate{
    
}
