//
//  SeeAllVC.swift
//  netflixClone
//
//  Created by Doble on 09/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class SeeAllVC: UIViewController {

    // Outlets
    @IBOutlet weak var videosCollection: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var nothingLbl: UILabel!
    
    // Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Title comes"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setUpColor()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpColor() {
        
        //DARK
        self.view.backgroundColor = Constants.CommonColors.theameDark
        self.emptyView.backgroundColor = Constants.CommonColors.theameDark
        self.videosCollection.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.nothingLbl.textColor = Constants.CommonColors.theameLight
    }
}

//MARK: Collectionview delegate and datasource
extension SeeAllVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width/3-16, height: UIScreen.main.bounds.width/3+30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            Constants.Cells.seeaAllCCell, for: indexPath) as? SeeAllCollectionCell {
            
            cell.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            return cell
            
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
}
