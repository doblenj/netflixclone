//
//  AccountVC.swift
//  netflixClone
//
//  Created by Doble on 06/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AccountVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: Variables
    var accountArray : [String] = [];
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var proImgView: UIView!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mailLbl: UILabel!
    
    @IBOutlet weak var deleteAccountView: UIView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var deleteAccountBtnView: UIView!
    @IBOutlet weak var deleteAccountBtn: UIButton!
    @IBOutlet weak var cancelBtnView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    
    //MARK: Actions
  
    @IBAction func cancelBtnTapped(_ sender: Any) {
        
        self.deleteAccountView.isHidden = true
    }
    
    @IBAction func editAccountTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.editAccountVC) as? EditAccountVC {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func showPasswordBtnTapped(_ sender: Any) {
        
        self.showPasswordBtn.isSelected = !self.showPasswordBtn.isSelected
        self.passwordTextField.isSecureTextEntry = self.showPasswordBtn.isSelected ? false : true
    }
    
    @IBAction func deleteAccountBtnTapped(_ sender: Any) {
        
        if isValidateParameters() {
            
            self.delay(bySeconds: 1.0) {
                
                self.performLogout(msg: "", Vc: self, isForcefull: false)
            }
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = StringConstants.account
        proImgView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: proImgView.frame.width/2, masksToBounds: true)
        
        if Common.isUserManuallyLogedIn() {
                
            accountArray = [StringConstants.AccountArray.viewPlans,StringConstants.AccountArray.myPlans,StringConstants.AccountArray.wishList,StringConstants.AccountArray.cards,StringConstants.AccountArray.myWallet,StringConstants.AccountArray.paidVideos,StringConstants.AccountArray.spamVideos,StringConstants.AccountArray.history,StringConstants.AccountArray.changePassword,StringConstants.AccountArray.deleteAccount];
            
        } else {

            accountArray = [StringConstants.AccountArray.viewPlans,StringConstants.AccountArray.myPlans,StringConstants.AccountArray.wishList,StringConstants.AccountArray.cards,StringConstants.AccountArray.myWallet,StringConstants.AccountArray.paidVideos,StringConstants.AccountArray.spamVideos,StringConstants.AccountArray.history,StringConstants.AccountArray.deleteAccount];
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.deleteAccountView.isHidden = true
        nameLbl.text = UserDefaults.standard.value(forKey: Constants.Keys.userNameKey) as? String
        mailLbl.text = UserDefaults.standard.value(forKey: Constants.Keys.emailIdKey) as? String
        self.proImg.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }
    
    //to validate the parameters
    
    func isValidateParameters() -> Bool {
        
        if passwordTextField.text == "" || passwordTextField.text == nil {
            
            self.view.makeToast(StringConstants.fieldsCantBeEmpty)
            return false
            
        } else {
            
            if ValidationClass.isValidPassword(password: passwordTextField.text ?? "") {
                
                return true
            } else {
                
                self.view.makeToast(StringConstants.passwordCharacterCountError)
                return false
            }
        }
    }
    
    //tableview delegate func
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.accountArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.accountCell, for: indexPath as IndexPath) as? AccountCell {
            
            cell.configureMoreCellWith(name: self.accountArray[indexPath.row], indexPath: indexPath)
            cell.selectionStyle = .none
            
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if accountArray[indexPath.row] == StringConstants.AccountArray.viewPlans { //View plans
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.viewPlansVC) as? ViewPlansVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.myPlans {// My plans
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.myPlansVC) as? MyPlansVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.wishList { // Wish list
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.videosListCommonVC) as? VideosListCommonVC {
                
                vc.titleString = StringConstants.AccountArray.wishList
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.cards { //Cards
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.cardsVC) as? CardsVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.paidVideos { //Paid Videos
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.paidVideosVC) as? PaidVideosVC {
                
                vc.titleString = StringConstants.AccountArray.paidVideos
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.spamVideos { //Spam Videos
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.videosListCommonVC) as? VideosListCommonVC {
                
                vc.titleString = StringConstants.AccountArray.spamVideos
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.history { //History
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.videosListCommonVC) as? VideosListCommonVC {
                
                vc.titleString = StringConstants.AccountArray.history
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.myWallet { //My Wallet
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.myWalletVC) as? MyWalletVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else if accountArray[indexPath.row] == StringConstants.AccountArray.deleteAccount { //Delete Account
            
            let alert = UIAlertController(title: Constants.appName, message: StringConstants.areYouSureToDeleteAccount, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: StringConstants.yes, style: UIAlertAction.Style.destructive, handler: { action in
                
                if Common.isUserManuallyLogedIn() {
                    
                    self.deleteAccountView.isHidden = false
                    self.headingLbl.text = StringConstants.enterPasswordToDelete
                    self.cancelBtnView.backgroundColor = Constants.CommonColors.theameLight
                    self.deleteAccountBtnView.backgroundColor = Constants.CommonColors.theameLight
                    self.cancelBtnView.setBorderProperties(borderColor: Constants.CommonColors.theameDark, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
                    self.deleteAccountBtnView.setBorderProperties(borderColor: Constants.CommonColors.theameDark, borderWidth: 1, cornerRadius: 7, masksToBounds: true)
                    self.cancelBtn.setTitleColor(Constants.CommonColors.theameDark, for: .normal)
                    self.deleteAccountBtn.setTitleColor(Constants.CommonColors.theameDark, for: .normal)
                } else {
                   
                    self.delay(bySeconds: 1.0) {
                        
                        self.performLogout(msg: "", Vc: self, isForcefull: false)
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: StringConstants.no, style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else if accountArray[indexPath.row] == StringConstants.AccountArray.changePassword { //Change password
            
            let sb = UIStoryboard.init(name: Constants.StoryboardIds.loginSb, bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.changePwdVC) as? ChangePasswordVC {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
