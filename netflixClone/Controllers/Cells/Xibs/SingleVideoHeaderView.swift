//
//  SingleVideoHeaderView.swift
//  netflixClone
//
//  Created by Doble on 18/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class SingleVideoHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var seasonBorderView: UIView!
    @IBOutlet weak var trailerBorderView: UIView!
    @IBOutlet weak var seasonBtn: UIButton!
    @IBOutlet weak var trailersBtn: UIButton!
    @IBOutlet weak var seasonSelectBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
}
