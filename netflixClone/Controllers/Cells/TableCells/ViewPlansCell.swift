//
//  ViewPlansCell.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class ViewPlansCell: UITableViewCell {
    
    //MARK:Variables
    
    
    //MARK:Outlets
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var backgroundAmountLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var noOfAccountLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var viewDetailsBtn: UIButton!
    
    //MARK: Actions
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        cellUIView.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
        viewDetailsBtn.setBorderProperties(borderColor: UIColor.white, borderWidth: 1, cornerRadius: 5, masksToBounds: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(indexPath : IndexPath) {
        
        self.backgroundAmountLbl.text = "$50"
        self.nameLbl.text = "Basic plan"
        self.amountLbl.text = "$50"
        self.noOfAccountLbl.text = "2"
        self.valueLbl.text = "3"
    }

}
