//
//  AppSettingsCell.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class AppSettingsCell: UITableViewCell {

    
    //MARK: Variables
    
    
    //MARK: Outlets
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var notiImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(name : String ,iconImg: String, notiImg: String, indexPath : IndexPath) {
        
        self.titleLbl.text = name
        self.iconImg.image = UIImage(named: iconImg)
        self.notiImg.image = UIImage(named: notiImg)
    }

}
