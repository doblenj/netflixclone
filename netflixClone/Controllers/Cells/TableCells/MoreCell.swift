//
//  MoreCell.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {

    
    
    //MARK: Variables
    
    
    //MARK: Outlets
    
    @IBOutlet weak var titleLbl: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpColor() {
        
        self.titleLbl.textColor = Constants.CommonColors.theameLight
        self.contentView.backgroundColor = Constants.CommonColors.theameDark
    }
    
    func configureMoreCellWith(name : String) {
        
        self.titleLbl.text = name
    }

}
