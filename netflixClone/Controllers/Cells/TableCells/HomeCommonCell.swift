//
//  HomeCommonCell.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

protocol HomeCommonCellDataSource: class {
    
    func numberOfItemsInCollection(in cell: UITableViewCell, at indexPath: IndexPath) -> Int
}

class HomeCommonCell: UITableViewCell {
    
    weak var datasource: HomeCommonCellDataSource?
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var sectionTitleLbl: UILabel!
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var contentUIView: UIView!
    
    var indexPath: IndexPath?
    var cellType: CellType = .defaultCell
    var sectionTitle: String = ""
    var currentVC = UIViewController()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Register collection cell
        videosCollectionView.register(UINib(nibName: Constants.Cells.homeDefaultCCell, bundle: nil), forCellWithReuseIdentifier: Constants.Cells.homeDefaultCCell)
        setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.contentUIView.backgroundColor = Constants.CommonColors.theameDark
        
        //LIGHT
        self.sectionTitleLbl.textColor = Constants.CommonColors.theameLight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // See all button action
    @IBAction func seeAllBtnTapped(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        if let seeAllVC = storyboard.instantiateViewController(withIdentifier: Constants.ViewControllers.seeAllVC) as? SeeAllVC {

            currentVC.navigationController?.pushViewController(seeAllVC, animated: true)
        }
    }
    
}

extension HomeCommonCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let index = indexPath {
            
            return datasource?.numberOfItemsInCollection(in: self, at: index) ?? 0
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 120, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.homeDefaultCCell, for: indexPath) as? HomeDefaultCollectionCell {
        
            cell.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }
        
        //In future, if cell type can change. So we can use the below method for cell types
        
//        switch cellType {
//        case .defaultCell:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.homeDefaultCCell, for: indexPath) as! HomeDefaultCollectionCell
//            return cell

//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentVC.navigateToSingleVideoPage(adminVideoID: 0)
    }
}
