//
//  WalletCell.swift
//  netflixClone
//
//  Created by Doble on 20/05/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {

    //MARK:Outlets
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var iconUIView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outerView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 8, masksToBounds: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(indexpath: IndexPath) {
        
        self.titleLbl.text = "paid for something"
        self.paymentLbl.text = "gdgd5gdv555sccs"
        self.dateLbl.text = "10-10-19"
        self.amountLbl.text = "$50"
    }

}
