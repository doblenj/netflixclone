//
//  PlayerTableCell.swift
//  netflixClone
//
//  Created by Doble on 14/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SDWebImage

class PlayerTableCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    
    // Variables
    var currentVC = UIViewController()
    var gradientLayer: CAGradientLayer = CAGradientLayer()

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpColor()
        let colorTop =  UIColor.black.cgColor
        let colorMiddle = UIColor.clear.cgColor
        let colorBottom = UIColor.black.cgColor
        gradientLayer.colors = [ colorTop, colorMiddle, colorBottom]
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        gradientLayer.frame = gradientView.bounds
    }
    
    func setUpColor() {
        
        self.titleLbl.textColor = Constants.CommonColors.theameLight
    }
    
    func updateUI() {
        
        self.titleLbl.text = "lorem ipsum"
        videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }

    // Button actions
    @IBAction func playBtnTapped(_ sender: Any) {
        
        let sb = UIStoryboard.init(name: Constants.StoryboardIds.moreSubSb, bundle: nil)
        
        if let vc = sb.instantiateViewController(withIdentifier: Constants.ViewControllers.playerVC) as? PlayerVC {
            
            currentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        currentVC.navigationController?.popViewController(animated: true)
    }
}
