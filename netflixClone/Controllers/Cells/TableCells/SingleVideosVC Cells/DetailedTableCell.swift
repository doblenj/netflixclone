//
//  DetailedTableCell.swift
//  netflixClone
//
//  Created by Doble on 14/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import TagListView

class DetailedTableCell: UITableViewCell, TagListViewDelegate {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var viewsCountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var likesCountLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var castCrewBackView: UIView!
    @IBOutlet weak var castCrewView: TagListView!
    @IBOutlet weak var castNameLbl: UILabel!
    
    // Variables
    var currentVC = UIViewController()
    var spamReasonsArr = ["reason 1","reason 2"]
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        let likesStr = StringConstants.likes
        self.likesCountLbl.text = "\(55) \(likesStr)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateUI() {
        
        titleLbl.text = "Title comes here"
        descriptionLbl.text = "Description comes here"
        viewsCountLbl.text = "\(50) views"
        dateLbl.text = "10-10-19"
        ageLbl.text = "25"
        likesCountLbl.text = "\(55) Likes"
        castNameLbl.isHidden = true
    }
    
    @IBAction func spamBtnTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: Constants.appName, message: StringConstants.reportSpam, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: StringConstants.yes, style: .default) { (UIAlertAction) in
            
            // Action sheet: Shows list of spam reasons
            self.openActionSheet()
        }
        let noAction = UIAlertAction(title: StringConstants.no, style: .cancel, handler: nil)
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        currentVC.present(alert, animated: true, completion: nil)

    }
    
    func openActionSheet() {
        
        let reasonAlert = UIAlertController(title: "", message: StringConstants.reasonForSpam, preferredStyle: .actionSheet)
        
        for item in spamReasonsArr {
            
            let action = UIAlertAction(title: item, style: .default) { (UIAlertAction) in
                
                DispatchQueue.main.async {
                    
                    self.currentVC.navigationController?.popViewController(animated: true)
                }
            }
            reasonAlert.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel, style: .cancel, handler: nil)
        
        reasonAlert.addAction(cancelAction)
        reasonAlert.modalPresentationStyle = .popover
        let popPresenter = reasonAlert.popoverPresentationController
        popPresenter?.sourceView = self.moreBtn
        popPresenter?.sourceRect = self.moreBtn.bounds
        currentVC.present(reasonAlert, animated: true, completion: nil)
    }
    
}
