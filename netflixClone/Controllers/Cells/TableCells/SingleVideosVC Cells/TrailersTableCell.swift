//
//  TrailersCell.swift
//  netflixClone
//
//  Created by Doble on 18/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class TrailersTableCell: UITableViewCell {

    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
