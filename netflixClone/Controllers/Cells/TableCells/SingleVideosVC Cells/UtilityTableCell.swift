//
//  UtilityTableCell.swift
//  netflixClone
//
//  Created by Doble on 14/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import UICircularProgressRing

class UtilityTableCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mylistBtn: UIButton!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var mylistImgView: UIImageView!
    @IBOutlet weak var rateImgView: UIImageView!
    @IBOutlet weak var downloadImgView: UIImageView!
    @IBOutlet weak var downloadLbl: UILabel!
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var progressView: UICircularProgressRing!
    
    // Variables
    var currentVC = UIViewController()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        downloadLbl.text = StringConstants.download
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func updateUI() {
        
        
    }
    
    // Button actions
    
    @IBAction func mylistBtnTapped(_ sender: UIButton) {
        
        self.currentVC.view.makeToast(StringConstants.pending)
    }
    
    @IBAction func rateBtnTapped(_ sender: UIButton) {
        
        self.currentVC.view.makeToast(StringConstants.pending)
    }
    
    @IBAction func shareBtnTapped(_ sender: Any) {
        
        let userName = UserDefaults.standard.value(forKey: Constants.Keys.userNameKey)
        
        // url to share
        let shareUrl = "Link comes here"
        
        // set up activity view controller
        let urlToShare = [shareUrl, "\(userName ?? "") Shared a link"]
        let activityViewController = UIActivityViewController(activityItems: urlToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = currentVC.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        currentVC.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func downloadBtnTapped(_ sender: Any) {
        
        self.currentVC.view.makeToast(StringConstants.pending)
    }
    
}
