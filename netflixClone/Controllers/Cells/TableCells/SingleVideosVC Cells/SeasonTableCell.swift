//
//  SeasonTableCell.swift
//  netflixClone
//
//  Created by Doble on 22/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class SeasonTableCell: UITableViewCell {

    
    //MARK: Variables
    
    
    //MARK: Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.mainView.backgroundColor = Constants.CommonColors.theameDark
        
        
        //LIGHT
        self.titleLbl.textColor = Constants.CommonColors.theameLight
        self.durationLbl.textColor = Constants.CommonColors.theameLight
        self.descLbl.textColor = Constants.CommonColors.theameLight
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
