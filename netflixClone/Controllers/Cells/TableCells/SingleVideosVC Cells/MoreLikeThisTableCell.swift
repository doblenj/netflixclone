//
//  MoreLikeThisTableCell.swift
//  netflixClone
//
//  Created by Doble on 18/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SDWebImage

class MoreLikeThisTableCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var noVideosLbl: UILabel!
    
    // Variables
    var currentVC = SingleVideoVC()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Register collection cell
        videosCollectionView.register(UINib(nibName: Constants.Cells.homeDefaultCCell, bundle: nil) , forCellWithReuseIdentifier: Constants.Cells.homeDefaultCCell)
        
        self.noVideosLbl.isHidden = true
        self.bringSubviewToFront(self.noVideosLbl)
        setUpColor()
    }
    
    func setUpColor() {
        
        //LIGHT
        self.noVideosLbl.textColor = Constants.CommonColors.theameLight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension MoreLikeThisTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width/3-20, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.homeDefaultCCell, for: indexPath) as? HomeDefaultCollectionCell {
            
            cell.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.currentVC.navigateToSingleVideoPage(adminVideoID: 0)
    }
}
