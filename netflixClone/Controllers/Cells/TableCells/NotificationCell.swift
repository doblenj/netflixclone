//
//  NotificationCell.swift
//  netflixClone
//
//  Created by Doble on 12/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    
    //MARK: Variables
    
    
    //MARK: Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeAgoLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(indexPath : IndexPath) {
        
        self.titleLbl.text = "Notification comes here"
        self.imgView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
        self.timeAgoLbl.text = "10 minutes ago"
    }

}
