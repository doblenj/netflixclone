//
//  HomeBannerCell.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit
import SDWebImage

protocol HomeBannerTableCellDataSource: class {
    
    func numberofItemsInBannerCollection(in cell: UITableViewCell, at indexPath:IndexPath) -> Int
}

class HomeBannerCell: UITableViewCell {

    @IBOutlet weak var bannerCollectionView: UICollectionView!
    
    //MARK: Variables
    weak var datasource: HomeBannerTableCellDataSource?
    var indexPath: IndexPath?
    var currentVC = UIViewController()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.bannerCollectionView.delegate = self
        self.bannerCollectionView.dataSource = self
        bannerCollectionView.register(UINib(nibName: Constants.Cells.homeBannerCCell, bundle: nil), forCellWithReuseIdentifier: Constants.Cells.homeBannerCCell)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    // Banner: Play button action
    @objc func playBtnTapped(sender: UIButton) {
        
        currentVC.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
    // Banner: Info button action
    @objc func infoBtnTapped(sender: UIButton) {
        
        currentVC.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
    // Banner: Add/Remove mylist button action
    @objc func mylistBtnTapped(sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        self.currentVC.view.makeToast(StringConstants.pending)
    }
}

extension HomeBannerCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let index = indexPath {
            
            return datasource?.numberofItemsInBannerCollection(in: self, at: index) ?? 0
        } else {
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width, height: UIScreen.main.bounds.height/1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.homeBannerCCell, for: indexPath) as? HomeBannerCollectionCell {
            
            cell.bannerImage.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
            cell.myListBtn.tag = indexPath.row
            cell.playBtn.tag = indexPath.row
            cell.infoBtn.tag = indexPath.row
            
            cell.myListBtn.isSelected = true
            cell.myListBtn.addTarget(self, action: #selector(mylistBtnTapped(sender:)), for: .touchUpInside)
            cell.playBtn
                .addTarget(self, action: #selector(playBtnTapped(sender:)), for: .touchUpInside)
            cell.infoBtn.addTarget(self, action: #selector(infoBtnTapped(sender:)), for: .touchUpInside)
            return cell
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentVC.navigateToSingleVideoPage(adminVideoID: 0)
    }
    
}
