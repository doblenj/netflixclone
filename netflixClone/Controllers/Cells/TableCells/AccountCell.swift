//
//  AccountCell.swift
//  netflixClone
//
//  Created by Doble on 06/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {

    //MARK:Variables
    
    @IBOutlet weak var titleLbl: UILabel!
    
    //MARK: Outlest
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(name : String, indexPath : IndexPath) {
        
        self.titleLbl.text = name
    }

}
