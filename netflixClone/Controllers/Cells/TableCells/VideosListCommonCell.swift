//
//  VideosListCommonCell.swift
//  netflixClone
//
//  Created by Doble on 09/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class VideosListCommonCell: UITableViewCell {

    @IBOutlet weak var videoDescriptionLbl: UILabel!
    @IBOutlet weak var videoTitleLbl: UILabel!
    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        setUpColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    func setUpColor() {
        
        self.videoDescriptionLbl.textColor = Constants.CommonColors.theameLight
        self.videoTitleLbl.textColor = Constants.CommonColors.theameLight
    }
    
    func configureMoreCellWith(indexPath : IndexPath) {
        
        self.videoDescriptionLbl.text = "description comes here"
        self.videoImgView.sd_setImage(with: URL(string: videoImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
        self.videoTitleLbl.text = "lorem ipsum"
    }
    
}
