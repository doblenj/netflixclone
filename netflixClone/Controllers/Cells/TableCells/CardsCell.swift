//
//  CardsCell.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class CardsCell: UITableViewCell {

    //MARK: variabkles
    
    //MARK: Outlets
    
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var cardNumLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var expiryLbl: UILabel!
    
    //Actions
    @IBAction func deleteBtnTapped(_ sender: Any) {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMoreCellWith(isDefault : Bool, indexPath : IndexPath) {
        
        self.cardNumLbl.text = "XXXX-XXXX-XXXX-4445"
        
        if isDefault {
          
            self.radioBtn.isSelected = true
        } else {
            
            self.radioBtn.isSelected = false
        }
        self.expiryLbl.text = "12/25"
    }

}
