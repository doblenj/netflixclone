//
//  WhoIsWatchingCCell.swift
//  netflixClone
//
//  Created by Doble on 12/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class WhoIsWatchingCCell: UICollectionViewCell {
    //MARK:Variables
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cellUIView: UIView!
    
    //MARK: Outlets
    
    override func awakeFromNib() {
        
        self.nameLbl.textColor = Constants.CommonColors.theameLight
        self.cellUIView.backgroundColor = Constants.CommonColors.theameDark
    }
    
    func configureMoreCellWith(indexPath : IndexPath) {
        
        self.nameLbl.text = "profile Name"
        self.imageView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }
}
