//
//  MoreCCell.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class MoreCCell: UICollectionViewCell {
    
    //MARK:Variables
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var imgUIView: UIView!
    
    //MARK: Outlets
    
    override func awakeFromNib() {
        
        setUpColor()
    }
    
    func setUpColor() {
        
        self.nameLbl.textColor = Constants.CommonColors.theameLight
    }
    
    func configureMoreCellWith(isDefault: Bool, indexPath : IndexPath) {
       
        if isDefault {
            
            imgUIView.setBorderProperties(borderColor: UIColor.white, borderWidth: 3, cornerRadius: imgUIView.frame.width/2, masksToBounds: true)
        } else {
            imgUIView.setBorderProperties(borderColor: UIColor.clear, borderWidth: 0, cornerRadius: imgUIView.frame.width/2, masksToBounds: true)
        }
        self.nameLbl.text = "Profile name"
        self.imageView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }
    
}
