//
//  HomeDefaultCollectionCell.swift
//  netflixClone
//
//  Created by Doble on 07/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class HomeDefaultCollectionCell: UICollectionViewCell {

    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpColor() {
        
        self.backView.backgroundColor = Constants.CommonColors.theameDark
    }

}
