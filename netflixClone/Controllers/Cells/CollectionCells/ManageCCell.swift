//
//  ManageCCell.swift
//  netflixClone
//
//  Created by Doble on 05/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class ManageCCell: UICollectionViewCell {
    
    //MARK:Variables
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cellUIView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var lblView: UIView!
    
    //MARK: Outlets
    
    override func awakeFromNib() {
        
      self.nameLbl.textColor = Constants.CommonColors.theameLight
        self.cellUIView.backgroundColor = Constants.CommonColors.theameDark
    }
    
    
    func configureMoreCellWith(isDefault : Bool, indexPath : IndexPath) {
        
        if isDefault {
            
            self.iconImageView.isHidden = false
        } else {
            
            self.iconImageView.isHidden = true
        }
        self.nameLbl.text = "Profile Name"
        self.imageView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "default"), options: .refreshCached, completed: nil)
    }
    
}
