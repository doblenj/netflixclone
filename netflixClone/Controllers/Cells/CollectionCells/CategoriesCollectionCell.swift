//
//  CategoriesCollectionCell.swift
//  netflixClone
//
//  Created by Doble on 08/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class CategoriesCollectionCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var collectionImgView: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var collectionTitleLbl: UILabel!
    
    var gradientLayer: CAGradientLayer = CAGradientLayer()
    
    override func awakeFromNib() {
        
        let colorTop =  UIColor.clear.cgColor
        let colorBottom = UIColor.black.cgColor
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
        setUpColor()
    }
    
    func setUpColor() {
        
        //LIGHT
        self.collectionTitleLbl.textColor = Constants.CommonColors.theameLight
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
    }
    
}
