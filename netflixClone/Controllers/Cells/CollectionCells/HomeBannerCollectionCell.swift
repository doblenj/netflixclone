//
//  HomeBannerCollectionCell.swift
//  netflixClone
//
//  Created by Doble on 12/03/19.
//  Copyright © 2019 codegama. All rights reserved.
//

import UIKit

class HomeBannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var myListBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var actionsView: UIView!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var playLbl: UILabel!
    
    
    var gradientLayer: CAGradientLayer = CAGradientLayer()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        infoBtn.imageView?.contentMode = .scaleAspectFit
        infoBtn.imageView?.clipsToBounds = true
        
        myListBtn.imageView?.contentMode = .scaleAspectFit
        myListBtn.imageView?.clipsToBounds = true
        
        let colorTop =  UIColor.black.cgColor
        let colorMiddle = UIColor.clear.cgColor
        let colorBottom = UIColor.black.cgColor
        gradientLayer.colors = [ colorTop, colorMiddle, colorBottom]
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
        
        setUpColor()
    }
    
    func setUpColor() {
        
        //DARK
        self.bannerImage.backgroundColor = Constants.CommonColors.theameDark
        self.playLbl.textColor = Constants.CommonColors.theameDark
        
        
        //LIGHT
        
        self.infoBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
        self.myListBtn.setTitleColor(Constants.CommonColors.theameLight, for: .normal)
       
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        backView.frame = self.bounds
        gradientLayer.frame = gradientView.bounds
    }

}
